﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClosedXML.Excel;

namespace Scheduler.Helpers
{
    public static class ReportHelper
    {
        public static void SetRowValues<T>(IXLWorksheet ws, int row, IEnumerable<T> values, int start_cell = 1, bool bold = false)
        {
            int cell_number = start_cell;
            foreach(T val in values)
            {
                ws.Cell(row, cell_number).Value = val;
                ws.Cell(row, cell_number).Style.Font.Bold = bold;
                cell_number++;
            }
        }

        public static void SetManyRowValues<T>(IXLWorksheet ws, int row, IEnumerable<IEnumerable<T>> values, int start_cell = 1)
        {
            int start_row = row;
            foreach(IEnumerable<T> val in values)
            {
                SetRowValues<T>(ws,start_row++, val, start_cell);
            }
        }
    }
}