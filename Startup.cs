﻿using Microsoft.Owin;
using Owin;
using Scheduler.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

[assembly: OwinStartupAttribute(typeof(Scheduler.Startup))]
namespace Scheduler
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
        }


        private void CreateRole(RoleManager<IdentityRole> roleManager, string rolename)
        {
            if (!roleManager.RoleExists(rolename))
            {
                var role = new IdentityRole();
                role.Name = rolename;
                roleManager.Create(role);

            }
        }

        // In this method we will create default User roles and Admin user for login  
        private void createRolesandUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // In Startup iam creating first Admin Role and creating a default Admin User    
            if (!roleManager.RoleExists("Super Admin"))
            {

                // first we create Admin rool   
                var role = new IdentityRole();
                role.Name = "Super Admin";
                roleManager.Create(role);

                //Here we create a Admin super user who will maintain the website                  

                var user = new ApplicationUser();
                user.UserName = "albertwild@gmail.com";
                user.Email = "albertwild@gmail.com";

                string userPWD = "edro.412";

                var chkUser = UserManager.Create(user, userPWD);

                //Add default User to Role Admin   
                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "Super Admin");

                }
            }

            // creating Creating Manager role
            string[] check_roles = new string[] { "Admin", "Planner", "DC Employee", "View Only" };
            foreach(var r in check_roles)
            {
                CreateRole(roleManager, r);
            }
        }
    }
}
