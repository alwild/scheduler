namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TWMCityName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Location", "TMWCityName", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Location", "TMWCityName");
        }
    }
}
