namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SlotDuration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Carrier",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.LocationConfigurationItem", "SlotDuration", c => c.Int(nullable: false));
            Sql("UPDATE dbo.LocationConfigurationItem SET SlotDuration=60");
        }
        
        public override void Down()
        {
            DropColumn("dbo.LocationConfigurationItem", "SlotDuration");
            DropTable("dbo.Carrier");
        }
    }
}
