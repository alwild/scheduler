namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CarrierAppointment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Appointment", "CarrierID", c => c.Int());
            CreateIndex("dbo.Appointment", "CarrierID");
            AddForeignKey("dbo.Appointment", "CarrierID", "dbo.Carrier", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Appointment", "CarrierID", "dbo.Carrier");
            DropIndex("dbo.Appointment", new[] { "CarrierID" });
            DropColumn("dbo.Appointment", "CarrierID");
        }
    }
}
