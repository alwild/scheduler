namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSlug : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Location", "Slug", c => c.String(maxLength: 255));
            CreateIndex("dbo.Location", "Slug", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Location", new[] { "Slug" });
            DropColumn("dbo.Location", "Slug");
        }
    }
}
