namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LocationConfigurationType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LocationConfiguration", "ConfigurationType", c => c.Int());
            Sql("UPDATE dbo.LocationConfiguration SET ConfigurationType=0");
        }
        
        public override void Down()
        {
            DropColumn("dbo.LocationConfiguration", "ConfigurationType");
        }
    }
}
