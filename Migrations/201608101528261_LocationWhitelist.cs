namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LocationWhitelist : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocationWhitelistSlots",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LocationID = c.Int(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Location", t => t.LocationID, cascadeDelete: true)
                .Index(t => t.LocationID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LocationWhitelistSlots", "LocationID", "dbo.Location");
            DropIndex("dbo.LocationWhitelistSlots", new[] { "LocationID" });
            DropTable("dbo.LocationWhitelistSlots");
        }
    }
}
