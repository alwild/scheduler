namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserProfiles : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        LocationID = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Location", t => t.LocationID, cascadeDelete: true)
                .Index(t => t.LocationID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserProfile", "LocationID", "dbo.Location");
            DropIndex("dbo.UserProfile", new[] { "LocationID" });
            DropTable("dbo.UserProfile");
        }
    }
}
