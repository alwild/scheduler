namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatedModified_Fields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Appointment", "CreatedDate", c => c.DateTime());
            AddColumn("dbo.Appointment", "CreatedBy", c => c.String());
            AddColumn("dbo.Appointment", "ModifiedDate", c => c.DateTime());
            AddColumn("dbo.Appointment", "ModifiedBy", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Appointment", "ModifiedBy");
            DropColumn("dbo.Appointment", "ModifiedDate");
            DropColumn("dbo.Appointment", "CreatedBy");
            DropColumn("dbo.Appointment", "CreatedDate");
        }
    }
}
