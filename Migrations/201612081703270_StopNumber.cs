namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StopNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Appointment", "StopNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Appointment", "StopNumber");
        }
    }
}
