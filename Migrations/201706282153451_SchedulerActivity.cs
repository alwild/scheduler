namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SchedulerActivity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SchedulerActivity",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ActivityType = c.String(),
                        ActivityDate = c.DateTime(nullable: false),
                        ActivityUserID = c.String(),
                        EntityID = c.Int(nullable: false),
                        PreviousValue = c.String(),
                        NewValue = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SchedulerActivity");
        }
    }
}
