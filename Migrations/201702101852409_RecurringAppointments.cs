namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RecurringAppointments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AppointmentRecurringSettings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        Sunday = c.Boolean(nullable: false),
                        Monday = c.Boolean(nullable: false),
                        Tuesday = c.Boolean(nullable: false),
                        Wednesday = c.Boolean(nullable: false),
                        Thursday = c.Boolean(nullable: false),
                        Friday = c.Boolean(nullable: false),
                        Saturday = c.Boolean(nullable: false),
                        FirstWeek = c.Boolean(nullable: false),
                        SecondWeek = c.Boolean(nullable: false),
                        ThirdWeek = c.Boolean(nullable: false),
                        FourthWeek = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Appointment", "RecurringSettingsID", c => c.Int());
            AddColumn("dbo.Appointment", "IsRecurring", c => c.Boolean());
            CreateIndex("dbo.Appointment", "RecurringSettingsID");
            AddForeignKey("dbo.Appointment", "RecurringSettingsID", "dbo.AppointmentRecurringSettings", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Appointment", "RecurringSettingsID", "dbo.AppointmentRecurringSettings");
            DropIndex("dbo.Appointment", new[] { "RecurringSettingsID" });
            DropColumn("dbo.Appointment", "IsRecurring");
            DropColumn("dbo.Appointment", "RecurringSettingsID");
            DropTable("dbo.AppointmentRecurringSettings");
        }
    }
}
