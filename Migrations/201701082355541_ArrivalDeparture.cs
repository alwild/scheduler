namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArrivalDeparture : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Appointment", "ArrivalDateTime", c => c.DateTime());
            AddColumn("dbo.Appointment", "DepartureDateTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Appointment", "DepartureDateTime");
            DropColumn("dbo.Appointment", "ArrivalDateTime");
        }
    }
}
