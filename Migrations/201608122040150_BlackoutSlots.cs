namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BlackoutSlots : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LocationBlackoutSlots", "Slots", c => c.Int(nullable: false));
            AddColumn("dbo.LocationWhitelistSlots", "Slots", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LocationWhitelistSlots", "Slots");
            DropColumn("dbo.LocationBlackoutSlots", "Slots");
        }
    }
}
