namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TMWDriverID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "TMWDriverID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "TMWDriverID");
        }
    }
}
