namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserProfileLocation : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserProfile", "LocationID", "dbo.Location");
            DropIndex("dbo.UserProfile", new[] { "LocationID" });
            AlterColumn("dbo.UserProfile", "LocationID", c => c.Int());
            CreateIndex("dbo.UserProfile", "LocationID");
            AddForeignKey("dbo.UserProfile", "LocationID", "dbo.Location", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserProfile", "LocationID", "dbo.Location");
            DropIndex("dbo.UserProfile", new[] { "LocationID" });
            AlterColumn("dbo.UserProfile", "LocationID", c => c.Int(nullable: false));
            CreateIndex("dbo.UserProfile", "LocationID");
            AddForeignKey("dbo.UserProfile", "LocationID", "dbo.Location", "ID", cascadeDelete: true);
        }
    }
}
