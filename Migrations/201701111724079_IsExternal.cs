namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsExternal : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Appointment", "IsExternal", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Appointment", "IsExternal");
        }
    }
}
