namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StatusColor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Status", "Color", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Status", "Color");
        }
    }
}
