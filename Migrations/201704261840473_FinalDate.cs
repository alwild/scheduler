namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FinalDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AppointmentRecurringSettings", "FinalDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AppointmentRecurringSettings", "FinalDate");
        }
    }
}
