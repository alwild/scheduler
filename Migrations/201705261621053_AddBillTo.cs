namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBillTo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "BillTo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "BillTo");
        }
    }
}
