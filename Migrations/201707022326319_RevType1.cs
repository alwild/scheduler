namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RevType1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfile", "RevType1", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfile", "RevType1");
        }
    }
}
