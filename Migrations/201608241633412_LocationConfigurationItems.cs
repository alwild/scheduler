namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LocationConfigurationItems : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocationConfigurationItem",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LocationConfigurationID = c.Int(nullable: false),
                        DayOfWeek = c.Int(nullable: false),
                        StartTime = c.Time(nullable: false, precision: 7),
                        EndTime = c.Time(nullable: false, precision: 7),
                        Slots = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.LocationConfiguration", t => t.LocationConfigurationID, cascadeDelete: true)
                .Index(t => t.LocationConfigurationID);
            
            DropColumn("dbo.LocationConfiguration", "StartTime");
            DropColumn("dbo.LocationConfiguration", "EndTime");
            DropColumn("dbo.LocationConfiguration", "Slots");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LocationConfiguration", "Slots", c => c.Int(nullable: false));
            AddColumn("dbo.LocationConfiguration", "EndTime", c => c.Time(nullable: false, precision: 7));
            AddColumn("dbo.LocationConfiguration", "StartTime", c => c.Time(nullable: false, precision: 7));
            DropForeignKey("dbo.LocationConfigurationItem", "LocationConfigurationID", "dbo.LocationConfiguration");
            DropIndex("dbo.LocationConfigurationItem", new[] { "LocationConfigurationID" });
            DropTable("dbo.LocationConfigurationItem");
        }
    }
}
