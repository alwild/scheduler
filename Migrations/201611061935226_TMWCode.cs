namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TMWCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Location", "TMWCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Location", "TMWCode");
        }
    }
}
