namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LocationConfiguration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocationBlackoutSlots",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LocationID = c.Int(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Location", t => t.LocationID, cascadeDelete: true)
                .Index(t => t.LocationID);
            
            CreateTable(
                "dbo.LocationConfiguration",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LocationID = c.Int(nullable: false),
                        StartTime = c.Time(nullable: false, precision: 7),
                        EndTime = c.Time(nullable: false, precision: 7),
                        Slots = c.Int(nullable: false),
                        PublishDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Location", t => t.LocationID, cascadeDelete: true)
                .Index(t => t.LocationID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LocationConfiguration", "LocationID", "dbo.Location");
            DropForeignKey("dbo.LocationBlackoutSlots", "LocationID", "dbo.Location");
            DropIndex("dbo.LocationConfiguration", new[] { "LocationID" });
            DropIndex("dbo.LocationBlackoutSlots", new[] { "LocationID" });
            DropTable("dbo.LocationConfiguration");
            DropTable("dbo.LocationBlackoutSlots");
        }
    }
}
