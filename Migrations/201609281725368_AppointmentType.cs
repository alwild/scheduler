namespace Scheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AppointmentType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Appointment", "AppointmentType", c => c.Int());
            Sql("UPDATE dbo.Appointment SET AppointmentType=0");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Appointment", "AppointmentType");
        }
    }
}
