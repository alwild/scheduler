﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;


namespace Scheduler.Models.TMW
{
    public class CarrierPayScheduleReport
    {
        [Column("Order Number")]
        public int OrderNumber { get; set; }
        [Column("Invoice Number")]
        public string InvoiceNumber { get; set; }
        [Column("Bill To")]
        public string BillTo { get; set; }
        [Column("RevType1")]
        public string RevType1 { get; set; }
        [Column("Invoice Created Date")]
        public DateTime? InvoiceCreatedDate { get; set; }
        [Column("35Days After Invoice Created Date")]
        public DateTime? DaysAfterInvoiceCreated { get; set; }
        [Column("Print Date")]
        public DateTime? PrintDate { get; set; }
        [Column("Child Order")]
        public int ChildOrder { get; set; }
        [Column("PayTo ID")]
        public string PayToID { get; set; }
        [Column("PayTo Name")]
        public string PayToName { get; set; }
        [Column("Carrier")]
        public string Carrier { get; set; }
        [Column("Carrier Name")]
        public string CarrierName { get; set; }
        [Column("Pay Description")]
        public string PayDescription { get; set; }
        [Column("Pay Amount")]
        public float PayAmount { get; set; }
        [Column("Pay Status")]
        public string PayStatus { get; set; }
        [Column("Pay Period")]
        public DateTime? PayPeriod { get; set; }


        public static IEnumerable<CarrierPayScheduleReport> GetReport(int order_number)
        {
            using (var db = new Database("TMW"))
            {
                db.EnableAutoSelect = false;
                return db.Fetch<CarrierPayScheduleReport>("exec CarrierPayScheduleReport @0", order_number);
            }
        }
    }
}