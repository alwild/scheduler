﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace Scheduler.Models.TMW
{
    public class DockReportModel
    {
        public DateTime Date { get; set; }
        public string City { get; set; }
        public string OrderNumber { get; set; }
        public string Carrier { get; set; }
        public string STONumber { get; set; }
        public string Status { get; set; }
        public string AppointmentType { get; set; }
        public string MoveNumber { get; set; }
        [PetaPoco.Ignore()]
        public bool ConsolidatedFirstOrder { get; set; }
        [PetaPoco.Ignore()]
        public bool ConsolidatedSubOrder { get; set; }
        public List<SubDockReportModel> ChildOrders { get; set; }
        public bool HasChildOrders
        {
            get
            {
                return this.ChildOrders != null && this.ChildOrders.Count > 0;
            }
        }
        public static IEnumerable<DockReportModel> GetReport(DateTime start, DateTime end, string city, string status, string appointmentType)
        {
            var sql = @"exec SchedulerOrdersReport @@StartDate=@StartDate, @@EndDate=@EndDate, @@City=@City, @@Status=@Status, @@AppointmentType=@AppointmentType";

            using (var db = new Database("TMW"))
            {
                db.EnableAutoSelect = false;
                var results = db.Fetch<DockReportModel, SubDockReportModel, DockReportModel>( 
                    new DockReportModelRelator().MapIt,
                    sql,
                    new { StartDate = start, EndDate = end, City = city, Status = status, AppointmentType = appointmentType })
                    .ToList();

                DockReportModel prev_item = null;
                foreach(var item in results)
                {
                    if (prev_item != null && prev_item.MoveNumber == item.MoveNumber)
                    {
                        prev_item.ConsolidatedFirstOrder = true;
                        item.ConsolidatedSubOrder = true;
                    }
                    else if (prev_item == null || prev_item.MoveNumber != item.MoveNumber)
                    {
                        prev_item = item;
                    }
                }

                var external_results = GetReportExternalOrders(start, end, city, status, appointmentType);

                if (external_results.Count() > 0)
                {
                    results.AddRange(external_results);
                    results.Sort((o, i) => o.Date.CompareTo(i.Date));
                }

                return results;
            }
        }

        public static IEnumerable<DockReportModel> GetReportExternalOrders(DateTime start, DateTime end, string city,
            string status, string appointmentType)
        {
            Scheduler.Models.DAL db = new Scheduler.Models.DAL();
            var query = db.Appointments.Where(a => a.IsExternal == true && a.StartTime >= start && a.EndTime < end);
            if (!String.IsNullOrEmpty(city))
            {
                query = query.Where(a => a.Location.TMWCityName == city);
            }

            if (!String.IsNullOrEmpty(appointmentType))
            {
                var cfgtype_index = Array.IndexOf(Appointment.TMWAppointmentType, appointmentType);
                query = query.Where(a => a.AppointmentType == (AppointmentType)cfgtype_index);
            }

            var results = new List<DockReportModel>();
            foreach (var item in query.ToList())
            {
                results.Add(new DockReportModel()
                {
                    AppointmentType = Appointment.TMWAppointmentType[(int)item.AppointmentType],
                    Carrier = (item.Carrier != null) ? item.Carrier.Name : "",
                    City = item.Location.TMWCityName,
                    Date = item.StartTime,
                    OrderNumber = item.OrderNumber,
                    Status = (item.Status != null) ? item.Status.Name : "",
                    STONumber = item.STONumber
                });
            }

            return results;
        }
    }

    public class SubDockReportModel
    {
        public DateTime? SubDate { get; set; }
        public string SubCity { get; set; }
        public string SubOrderNumber { get; set; }
        public string SubCarrier { get; set; }
        public string SubSTONumber { get; set; }
        public string SubStatus { get; set; }
        public string SubAppointmentType { get; set; }
    }

    class DockReportModelRelator
    {
        public DockReportModel current;
        public DockReportModel MapIt(DockReportModel parent, SubDockReportModel child)
        {
            // Terminating call.  Since we can return null from this function
            // we need to be ready for PetaPoco to callback later with null
            // parameters
            if (parent == null)
                return current;

            // Is this the same author as the current one we're processing
            if (current != null && current.OrderNumber == parent.OrderNumber)
            {
                // Yes, just add this post to the current author's collection of posts
                if (!String.IsNullOrEmpty(child.SubOrderNumber))
                {
                    current.ChildOrders.Add(child);
                }

                // Return null to indicate we're not done with this author yet
                return null;
            }

            // This is a different author to the current one, or this is the 
            // first time through and we don't have an author yet

            // Save the current author
            var prev = current;

            // Setup the new current author
            current = parent;
            current.ChildOrders = new List<SubDockReportModel>();
            if (!String.IsNullOrEmpty(child.SubOrderNumber))
            {
                current.ChildOrders.Add(child);
            }

            // Return the now populated previous author (or null if first time through)
            return prev;
        }
    }



    public class TMWOrderStatus
    {
        public string ord_status { get; set; }

        public static IEnumerable<TMWOrderStatus> Get()
        {
            var sql = "select distinct ord_status from orderheader";
            using (var db = new Database("TMW"))
            {
                return db.Fetch<TMWOrderStatus>(sql);
            }
        }
    }

    public class TMWAppointmentType
    {
        public string app_type { get; set; }

        public static IEnumerable<TMWAppointmentType> Get()
        {
            var ret = new List<TMWAppointmentType>();
            ret.Add(new TMWAppointmentType()
            {
                app_type = "LUL"
            });
            ret.Add(new TMWAppointmentType()
            {
                app_type = "LLD"
            });

            return ret;
        }
    }
}