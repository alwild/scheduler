﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;
using Scheduler.TMWService;

namespace Scheduler.Models.TMW
{
    public class TMWException : Exception
    {
        public TMWException(string message) : base("TMW ERROR: " + message) { }
    }

    public class TMWOrderEvent
    {
        public int ord_hdrnumber { get; set; }
        public string evt_eventcode { get; set; }
        public int evt_number { get; set; }
        public DateTime evt_earlydate { get; set; }
        public DateTime evt_latedate { get; set; }
        public string evt_carrier { get; set; }
        public int stp_city { get; set; }
        public string stp_companyid { get; set; }
        public string cty_nmstct { get; set; }
        public int stp_number { get; set; }
        public string sto_number { get; set; }
        public int move_number { get; set; }
        public int sequence { get; set; }

        [PetaPoco.Ignore]
        public Location ScheduleLocation { get; set; }

        /*
        public static IEnumerable<TMWOrderEvent> GetEvents(int ord_hdrnumber)
        {
            using (var db = new Database("TMW"))
            {
                return db.Fetch<TMWOrderEvent>(@"SELECT s.stp_city, c.cty_nmstct, e.* FROM [event] e
inner join stops s on s.stp_number=e.stp_number
inner join city c on c.cty_code=s.stp_city
WHERE e.ord_hdrnumber=@0", ord_hdrnumber);
            }
        }

        public static void Update(TMWOrderEvent evt)
        {
            using (var db = new Database("TMW"))
            {
                db.Execute("UPDATE [event] SET evt_earlydate=@0, evt_latedate=@1 WHERE evt_number=@2", evt.evt_earlydate, evt.evt_latedate, evt.evt_number);
            }
        }
        */

        private static Order GetOrderByID(int ord_hdrnumber)
        {
            Order ret = null;
            var client = new TMWService.APIWCFServicesClient();
            var result = client.GetOrder(ord_hdrnumber);
            if (result.ReturnCode == 0)
            {
                ret = result.ReferenceObjects[0];
            }
            else if (result.ReturnCode != 100)
            {
                throw new TMWException(String.Join("\r\n", result.ErrorMessages));
            }

            return ret;
        }

        public static Order GetOrder(string lookup, string lookup_type="ord")
        {
            var client = new TMWService.APIWCFServicesClient();
            Order ret = null;
            if (lookup_type == "ord")
            {
                int ord_hdrnumber = 0;
                if (Int32.TryParse(lookup, out ord_hdrnumber))
                {
                    var result = client.GetOrder(ord_hdrnumber);
                    if (result.ReturnCode == 0)
                    {
                        return result.ReferenceObjects[0];
                    }
                    else if (result.ReturnCode != 100)
                    {
                        throw new TMWException(String.Join("\r\n", result.ErrorMessages));
                    }
                }
            }
            if (lookup_type == "ref")
            {
                var result = client.RetrieveOrder(new Order.Criteria()
                {
                    ReferenceNumber1 = lookup
                });
                if (result.ReturnCode == 0)
                {
                    return result.ReferenceObjects[0];
                }
                else if (result.ReturnCode != 100)
                {
                    throw new TMWException(String.Join("\r\n", result.ErrorMessages));
                }
            }

            return ret;
        }

        public static IEnumerable<Order> GetOrdersByRef(string lookup)
        {
            var client = new TMWService.APIWCFServicesClient();
            var result = client.RetrieveOrder(new Order.Criteria()
            {
                ReferenceNumber1 = lookup
            });
            if (result.ReturnCode == 0)
            {
                return result.ReferenceObjects.ToList();
            }
            else if (result.ReturnCode != 100)
            {
                throw new TMWException(String.Join("\r\n", result.ErrorMessages));
            }
            return null;
        }

        public static IEnumerable<TMWOrderEvent> GetStops(string ord_hdrnumber, string lookup_type="ord", 
            string configtype = null, string revtype1=null)
        {
            var order = GetOrder(ord_hdrnumber, lookup_type);
            if(order == null)
            {
                throw new TMWException(String.Format("Could not find order for criteria: {0}", ord_hdrnumber));
            }

            if (!String.IsNullOrEmpty(revtype1) && order.RevType1 != revtype1)
            { 
                throw new TMWException(String.Format("Could not find order for criteria: {0}", ord_hdrnumber));
            }

            var ret = new List<TMWOrderEvent>();
            foreach (var stop in order.Stops)
            {
                if (String.IsNullOrEmpty(configtype) || stop.EventCode == configtype)
                {
                    string sto_number = "";
                    if (stop.ReferenceNumbers.Count() > 0)
                    {
                        sto_number = stop.ReferenceNumbers.First().Value;
                    }
                    else
                    {
                        var child_order = GetOrderByID(stop.ord_hdrnumber);
                        sto_number = child_order.ReferenceNumber1;
                    }
                    var evt = new TMWOrderEvent()
                    {
                        ord_hdrnumber = stop.ord_hdrnumber,
                        evt_carrier = stop.Carrier,
                        evt_earlydate = stop.EarliestDate,
                        evt_latedate = stop.LatestDate,
                        evt_number = stop.StopNumber,
                        evt_eventcode = stop.EventCode,
                        cty_nmstct = stop.CityName,
                        stp_number = stop.StopNumber,
                        stp_city = stop.City,
                        stp_companyid = stop.CompanyID,
                        sto_number = sto_number,
                        move_number = stop.MoveNumber,
                        sequence = stop.Sequence
                    };

                    ret.Add(evt);
                }
            }
            return ret;
        }

        public static TMWOrderEvent GetStop(int stp_number)
        {
            var client = new TMWService.APIWCFServicesClient();
            var result = client.GetStopByID(stp_number);

            if (result.ReturnCode == 0)
            {
                var stop = result.ReferenceObjects[0];
                var evt = new TMWOrderEvent()
                {
                    ord_hdrnumber = stop.ord_hdrnumber,
                    evt_carrier = stop.Carrier,
                    evt_earlydate = stop.EarliestDate,
                    evt_latedate = stop.LatestDate,
                    evt_number = stop.StopNumber,
                    evt_eventcode = stop.EventCode,
                    cty_nmstct = stop.CityName,
                    stp_number = stop.StopNumber,
                    stp_city = stop.City,
                    stp_companyid = stop.CompanyID,
                    move_number = stop.MoveNumber,
                    sequence = stop.Sequence
                };

                return evt;
            }

            return null;
        }

        public static void UpdateOrderStops(int ord_hdrnumber, string evt_eventcode, string cty_nmstct,
            DateTime appointmentdate, DateTime? arrival, DateTime? departure, string comment)
        {
            var svc = new TMWService.APIWCFServicesClient();
            var result = svc.GetOrder(ord_hdrnumber);
            var order = result.ReferenceObjects[0];
            var stops = order.Stops.Where(s => s.CityName == cty_nmstct && s.EventCode == evt_eventcode);
            using (var db = new Database("TMW"))
            {
                foreach (var stop in stops)
                {
                    stop.UpdateableProperties.EarliestDate = true;
                    stop.UpdateableProperties.LatestDate = true;
                    stop.UpdateableProperties.ArrivalDate = true;
                    stop.UpdateableProperties.DepartureDate = true;
                    stop.EarliestDate = appointmentdate;
                    stop.LatestDate = appointmentdate;
                    stop.ArrivalDate = appointmentdate;
                    stop.DepartureDate = appointmentdate;
                    stop.Comment = comment;
                    db.Execute(@"UPDATE [stops] set stp_custpickupdate = @0, stp_custdeliverydate = @1 WHERE stp_number = @2",
                                    arrival, departure, stop.StopNumber);
                }
            }
            order.UpdateableProperties.Stops = true;
            var ret = svc.SaveOrder(new Order[] { order }, true);
            CheckErrors(ret);
        }

        public static void UpdateStop(int ord_hdrnumber, int stp_number, 
            DateTime earlydate, DateTime latedate, DateTime? arrival, DateTime? departure,
            string comment)
        {
            var svc = new TMWService.APIWCFServicesClient();
            var result = svc.GetStop(ord_hdrnumber, stp_number);
            var stop = result.ReferenceObjects[0];
            stop.UpdateableProperties.EarliestDate = true;
            stop.UpdateableProperties.LatestDate = true;
            stop.UpdateableProperties.ArrivalDate = true;
            stop.UpdateableProperties.DepartureDate = true;
            stop.EarliestDate = earlydate;
            stop.LatestDate = latedate;
            stop.ArrivalDate = earlydate;
            stop.DepartureDate = latedate;
            stop.Comment = comment;
            var ret = svc.UpdateStop(stop.ord_hdrnumber, stop, TMWService.ListOperation.Add, stop.StopNumber);
            CheckErrors(ret);

            using (var db = new Database("TMW"))
            {
                db.Execute(@"UPDATE [stops] set stp_custpickupdate = @0, stp_custdeliverydate = @1 WHERE stp_number = @2",
                    arrival, departure, stp_number);
            }

            /*
            var parms = new List<Procedure.Parameter>();
            parms.Add(new Procedure.Parameter() { Name = "@stp_number", Value = Convert.ToString(stp_number) });
            if (arrival.HasValue)
            {
                parms.Add(new Procedure.Parameter()
                {
                    Name = "@pickupdate", Value = arrival.Value.ToLongDateString()
                });
            }

            if (departure.HasValue)
            {
                parms.Add(new Procedure.Parameter()
                {
                    Name = "@deliverydate",
                    Value = departure.Value.ToLongDateString()
                });
            }

            var call_ret = svc.CallProcedure("sch_UpdateStopCustomerDates", parms.ToArray());
            CheckErrors(call_ret);
            */
        }

        private static void CheckErrors(Scheduler.TMWService.APIClassReturnObjectBase ret)
        {
            if (ret.ReturnCode != 0)
            {
                if (ret.ErrorMessages.Count() > 0)
                {
                    throw new TMWException(ret.ErrorMessages[0]);
                }
                else if (ret.WarningMessages.Count() > 0)
                {
                    throw new TMWException(ret.WarningMessages[0]);
                }

                throw new TMWException("Unknown error saving data to TMW");
            }
        }

        public static void TestThing()
        {
            var svc = new TMWService.APIWCFServicesClient();
            
        }
    }

    public class TMWOrder
    {
        public string ord_hdrnumber { get; set; }
        public string ord_refnum { get; set; }
        public string ord_carrier { get; set; }
        public string mov_number { get; set; }

        public static void FillInTMWInfo(IEnumerable<Appointment> appointments)
        {
            using (var db = new Database("TMW"))
            {
                var order_numbers = appointments.Select(a => a.OrderNumber).ToArray();
                var sql = @"select ord_hdrnumber, ord_refnum, ord_carrier, mov_number from orderheader 
where mov_number in (select mov_number from orderheader where ord_hdrnumber in (@order_numbers))";
                var tmw_infos = db.Fetch<TMWOrder>(sql, new { order_numbers });
                foreach(var apt in appointments)
                {
                    var tmw_info = tmw_infos.FirstOrDefault(t => t.ord_hdrnumber == apt.OrderNumber);
                    if(tmw_info != null)
                    {
                        apt.STONumber = tmw_info.ord_refnum;
                        apt.CarrierName = tmw_info.ord_carrier;
                        apt.TMWOrders = new List<TMWOrder>();
                        //get the rest of the orders in the move
                        var consolidated_orders = tmw_infos.Where(t => t.mov_number == tmw_info.mov_number);
                        apt.TMWOrders.AddRange(consolidated_orders);
                    }
                }
            }
        }

        public static IEnumerable<TMW.TMWOrder> OrdersInMove(int mov_number)
        {
            using (var db = new Database("TMW"))
            {
                var sql = @"select ord_hdrnumber, ord_refnum, ord_carrier, mov_number from orderheader 
where mov_number=@0";
                var tmw_infos = db.Fetch<TMWOrder>(sql, mov_number);
                return tmw_infos;
            }
        }
    }
}