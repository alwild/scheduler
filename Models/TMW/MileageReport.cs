﻿using System;
using System.Collections.Generic;
using PetaPoco;

namespace Scheduler.Models.TMW
{
    public class MileageReport
    {
        public string DriverLogon { get; set; }
        public int Year { get; set; }
        public string Month { get; set; }
        public int MonthNumber { get; set; }
        public string TruckName { get; set; }
        public string Division { get; set; }
        public double MPG { get; set; }
        public double IDLEPCT { get; set; }
        public double EmptyMiles { get; set; }
        public double LoadedMiles { get; set; }

        public static IEnumerable<MileageReport> GetReport(DateTime start, DateTime end, string driverid)
        {
            using (var db = new Database("TMW"))
            {
                db.EnableAutoSelect = false;
                return db.Fetch<MileageReport>(";EXEC ShipEx_MileageReport @@BEGINDATE=@BEGINDATE, @@ENDDATE=@ENDDATE, @@DRIVERID=@DRIVERID",
                    new { BEGINDATE = start, ENDDATE = end, DRIVERID = driverid }
                    );
            }
        }
    }
}