﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PetaPoco;

namespace Scheduler.Models.TMW
{
    public class LoadStatusReport
    {
        public string OrderNumber { get; set; }
        public string Truck { get; set; }
        public string TruckDisplay
        {
            get
            {
                if (Truck == "UNKNOWN") return "Call Dispatch";
                return Truck;
            }
        }
        public string Trailer { get; set; }
        public string TrailerDisplay
        {
            get
            {
                if (Trailer == "UNKNOWN") return "Call Dispatch";
                return Trailer;
            }
        }
        public string Status { get; set; }
        public string StatusDescription { get; set; }
        public string StatusDisplay
        {
            get
            {
                if (String.IsNullOrEmpty(StatusDescription)) return Status;
                return StatusDescription;
            }
        }
        private string _location;
        public string Location
        {
            get
            {
                if (!String.IsNullOrEmpty(_location))
                    return _location.Replace("[IGN:X]", "");
                return _location;
            }
            set
            {
                _location = value;
            }
        }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string MaxTemp { get; set; }
        public string MaxTempDisplay
        {
            get
            {
                if (MaxTemp == "999") return "Dry Load";
                return MaxTemp;
            }
        }
        public string PONumber { get; set; }
        public decimal? Lumper { get; set; }
        public decimal? LHF { get; set; }

        public static IEnumerable<LoadStatusReport> GetReport(string[] po_number, string billto = null)
        {
            /*
            var orders = TMWOrderEvent.GetOrdersByRef(Convert.ToString(po_number));
            var ret = new List<LoadStatusReport>();
            if (orders != null)
            {
                foreach(var order in orders)
                {
                    var item = new LoadStatusReport()
                    {
                        Destination = order.DestPoint,
                        Origin = order.OriginPoint,
                        Status = order.StatusDisplay,
                        Trailer = order.Trailer,
                        Truck = order.Tractor
                    };
                    ret.Add(item);
                }
            }
            return ret;
            */
            using (var db = new Database("TMW"))
            {
//                var sql = @"
//  select ord_hdrnumber as OrderNumber, ord_maxtemp as MaxTemp,
//    ord_tractor as Truck,
//    ord_trailer as Trailer,
//    ord_refnum as PONumber,
//    dst_city.cty_nmstct as Destination,
//    org_city.cty_nmstct as Origin,
//    o.ord_status as [Status],
//    st.ord_status_description [StatusDescription],
//    trc.trc_gps_desc as [Location],
//    (select top 1 ivd_rate as LHF_Rate from invoicedetail where cht_itemcode='LHF' and ord_hdrnumber = o.ord_hdrnumber) as LHF,
//    (select top 1 ivd_rate as Lumper_Rate from invoicedetail where cht_itemcode='Lumper' and ord_hdrnumber = o.ord_hdrnumber) as Lumper
//from orderheader o
//  inner join city dst_city on dst_city.cty_code = ord_destcity
//  inner join city org_city on org_city.cty_code = ord_origincity
//  left join tractorprofile trc on trc.trc_number = ord_tractor
//  left join ShipEXOrderStatus st on st.ord_status = o.ord_status
//where ord_refnum in (@po_number) and o.ord_status <> 'CAN' and (@billto is null or ord_billto=@billto)";
                
                var sql = "EXEC ShipEx_LoadStatus_Report @@po_number=@po_number, @@billto=@billto";
                db.EnableAutoSelect = false;
                var ret = new List<LoadStatusReport>();
                foreach (var po in po_number)
                {
                    ret.AddRange(db.Fetch<LoadStatusReport>(sql, new { po_number = po, billto = billto }));
                }

                return ret;
            }
        }

    }
}