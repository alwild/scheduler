﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Scheduler.Models
{
    public class AppointmentRecurringSettings
    {
        public AppointmentRecurringSettings() { }

        public void SetRecurringFromAppointment(Appointment appointment)
        {
            this.StartTime = appointment.StartTime;
            this.EndTime = appointment.EndTime;
            this.Sunday = appointment.StartTime.DayOfWeek == DayOfWeek.Sunday;
            this.Monday = appointment.StartTime.DayOfWeek == DayOfWeek.Monday;
            this.Tuesday = appointment.StartTime.DayOfWeek == DayOfWeek.Tuesday;
            this.Wednesday = appointment.StartTime.DayOfWeek == DayOfWeek.Wednesday;
            this.Thursday = appointment.StartTime.DayOfWeek == DayOfWeek.Thursday;
            this.Friday = appointment.StartTime.DayOfWeek == DayOfWeek.Friday;
            this.Saturday = appointment.StartTime.DayOfWeek == DayOfWeek.Saturday;
        }

        public int ID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        [Display(ShortName ="Sun")]
        public bool Sunday { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool FirstWeek { get; set; }
        public bool SecondWeek { get; set; }
        public bool ThirdWeek { get; set; }
        public bool FourthWeek { get; set; }
        public DateTime? FinalDate { get; set; }

        public string FinalDateStr
        {
            get
            {
                if (FinalDate != null)
                {
                    return FinalDate.Value.ToShortDateString();
                }
                return String.Empty;
            }
        }

        public IEnumerable<DayOfWeek> GetDaysOfWeek()
        {
            List<DayOfWeek> selectedDays = new List<DayOfWeek>();
            if (Sunday) selectedDays.Add(DayOfWeek.Sunday);
            if (Monday) selectedDays.Add(DayOfWeek.Monday);
            if (Tuesday) selectedDays.Add(DayOfWeek.Tuesday);
            if (Wednesday) selectedDays.Add(DayOfWeek.Wednesday);
            if (Thursday) selectedDays.Add(DayOfWeek.Thursday);
            if (Friday) selectedDays.Add(DayOfWeek.Friday);
            if (Saturday) selectedDays.Add(DayOfWeek.Saturday);

            return selectedDays;
        }

        public IEnumerable<DateTime[]> GetDates()
        {
            var selectedDays = this.GetDaysOfWeek();
            var datediff = this.EndTime - this.StartTime;
            var curr = this.StartTime.AddDays(1);
            while(curr <= this.FinalDate.Value)
            {
                if (selectedDays.Contains(curr.DayOfWeek))
                {
                    var next_end = curr.Add(datediff);
                    yield return new DateTime[] { curr, next_end };
                }
                curr = curr.AddDays(1);
            }
        }

        public void UpdateFrom(AppointmentRecurringSettings recurring_settings)
        {
            this.Monday = recurring_settings.Monday;
            this.FirstWeek = recurring_settings.FirstWeek;
            this.EndTime = recurring_settings.EndTime;
            this.FourthWeek = recurring_settings.FourthWeek;
            this.Friday = recurring_settings.Friday;
            this.Saturday = recurring_settings.Saturday;
            this.SecondWeek = recurring_settings.SecondWeek;
            this.StartTime = recurring_settings.StartTime;
            this.Sunday = recurring_settings.Sunday;
            this.ThirdWeek = recurring_settings.ThirdWeek;
            this.Thursday = recurring_settings.Thursday;
            this.Tuesday = recurring_settings.Tuesday;
            this.Wednesday = recurring_settings.Wednesday;
            this.FinalDate = recurring_settings.FinalDate;
        }
    }
}