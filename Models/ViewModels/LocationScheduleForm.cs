﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scheduler.Models.ViewModels
{
    public class LocationScheduleForm
    {
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public string City { get; set; }
        public string Status { get; set; }
        public string AppointmentType { get; set; }
    }
}