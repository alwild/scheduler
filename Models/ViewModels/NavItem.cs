﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scheduler.Models.ViewModels
{
    public class NavItem
    {
        public NavItem(string text, string actionname, string controllername)
        {
            Text = text;
            ActionName = actionname;
            ControllerName = controllername;
        }

        public string Text { get; set; }
        public string ActionName { get; set; }
        public string ControllerName { get; set; }


        public static IEnumerable<NavItem> GetNavItemsForRole(IEnumerable<string> roles)
        {
            var ret = new List<NavItem>();
            ret.Add(new NavItem("Orders", "Order", "Calendar"));
            ret.Add(new NavItem("External", "ExternalOrder", "Calendar"));
            ret.Add(new NavItem("Report", "LocationSchedule", "Report"));

            if (roles.Contains("Super Admin") || roles.Contains("Admin"))
            {
                ret.Add(new NavItem("Locations", "Index", "Locations"));
            }

            if (roles.Contains("Super Admin"))
            {
                ret.Add(new NavItem("Carriers", "Index", "Carriers"));
                ret.Add(new NavItem("Statuses", "Index", "Status"));
                ret.Add(new NavItem("Users", "Index", "Users"));
            }

            return ret;
        }
    }
}