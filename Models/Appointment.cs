﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Scheduler.Models
{

    public enum AppointmentType { Unloading, Loading }

    public class Appointment
    {
        public int ID { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string OrderNumber { get; set; }
        public string StopNumber { get; set; }
        public string STONumber { get; set; }
        public int LocationID { get; set; }
        public int StatusID { get; set; }
        public int? CarrierID { get; set; }
        public int? RecurringSettingsID { get; set; }

        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }
        public DateTime? ArrivalDateTime { get; set; }
        public DateTime? DepartureDateTime { get; set; }
        public bool? IsExternal { get; set; }

        [Display(Name="Recurring")]
        public bool? IsRecurring { get; set; }

        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

        public AppointmentType? AppointmentType { get; set; }
        public virtual Location Location { get; set; }
        public virtual Status Status { get; set; }
        public virtual Carrier Carrier { get; set; }
        public virtual AppointmentRecurringSettings RecurringSettings { get; set; }

        [PetaPoco.Ignore]
        public bool IsCanceled
        {
            get
            {
                return (this.Status != null && this.Status.Name == "Canceled");
            }
        }

        [PetaPoco.Ignore]
        [NotMapped]
        public string CarrierName
        {
            get
            {
                if (this.Carrier != null) return this.Carrier.Name;
                return "";
            }
            set
            {
                if (this.Carrier == null)
                {
                    this.Carrier = new Carrier();
                }
                this.Carrier.Name = value;
            }
        }

        [PetaPoco.Ignore]
        public string ArrivalDateTimeStr
        {
            get
            {
                if (ArrivalDateTime.HasValue)
                {
                    return ArrivalDateTime.Value.ToString("MM/dd/yyyy HH:mm");
                }
                return "";
            }
        }

        [PetaPoco.Ignore]
        public string DepartureDateTimeStr
        {
            get
            {
                if (DepartureDateTime.HasValue)
                {
                    return DepartureDateTime.Value.ToString("MM/dd/yyyy HH:mm");
                }
                return "";
            }
        }

        [PetaPoco.Ignore]
        [NotMapped]
        public List<TMW.TMWOrder> TMWOrders { get; set; }
        public string OrderNumberDisplay
        {
            get
            {
                return (TMWOrders == null)?this.OrderNumber:String.Join(",", this.TMWOrders.Select(t => t.ord_hdrnumber));
            }
        }
        public string STONumberDisplay
        {
            get
            {
                return (TMWOrders == null)?this.STONumber:String.Join(",", this.TMWOrders.Select(t => t.ord_refnum));
            }
        }
        public string CarrierDisplay
        {
            get
            {
                return (TMWOrders == null)?this.CarrierName:String.Join(",", this.TMWOrders.Select(t => t.ord_carrier));
            }
        }

        public static string[] TMWAppointmentType = new string[] { "LUL", "LLD" };

        public void CopyFrom(Appointment appointment)
        {
            this.StartTime = appointment.StartTime;
            this.EndTime = appointment.EndTime;
            this.OrderNumber = appointment.OrderNumber;
            this.LocationID = appointment.LocationID;
            this.StatusID = appointment.StatusID;
            this.STONumber = appointment.STONumber;
            this.CarrierID = appointment.CarrierID;
            this.AppointmentType = appointment.AppointmentType;
            this.StopNumber = appointment.StopNumber;
            this.Comments = appointment.Comments;
            this.ArrivalDateTime = appointment.ArrivalDateTime;
            this.DepartureDateTime = appointment.DepartureDateTime;
            this.IsExternal = appointment.IsExternal;
            this.IsRecurring = appointment.IsRecurring;
            
            //RecurringSettingID is not updated via this method.  It is instead updated in the controller
            //this.RecurringSettingsID = appointment.RecurringSettingsID;
        }

        public void CopyDates(Appointment appointment)
        {
            this.ModifiedBy = appointment.ModifiedBy;
            this.ModifiedDate = appointment.ModifiedDate;
            this.CreatedBy = appointment.CreatedBy;
            this.CreatedDate = appointment.CreatedDate;
        }
    }

    public class MappedSchedule: Dictionary<DateTime, IList<Appointment>>
    {
        public IList<Appointment> GetAppointments(DateTime start)
        {
            return (this.ContainsKey(start) ? this[start] : null);
        }
    }
}