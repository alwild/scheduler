﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scheduler.Models
{
    public class Status
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
    }
}