using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Linq;

namespace Scheduler.Models
{
    public enum LocationConfigurationType { Unloading, Loading }

    public class LocationConfiguration
    {
        public LocationConfiguration()
        {
            this.Items = new List<LocationConfigurationItem>();
        }

        public int ID { get; set; }
        public int LocationID { get; set; }
        public virtual Location Location { get; set; }
        [Display(Name = "Publish Date")]
        public DateTime PublishDate { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string PublishDateString { get { return PublishDate.ToShortDateString(); } } 
        [Display(Name = "Type")]
        public LocationConfigurationType? ConfigurationType { get; set; }
        public virtual ICollection<LocationConfigurationItem> Items { get; set; }
        
        public bool IsBusinessHour(DateTime checktime)
        {
            throw new NotImplementedException();
        }

        public List<LocationConfigurationItem> ScheduleItems(int dayofweek)
        {
            var items = Items as List<LocationConfigurationItem>;
            return items.FindAll(i => i.DayOfWeek == dayofweek);
        }

        public IList<LocationConfigurationItem> GetSlots(DateTime checkdate)
        {
            var checkDoW = (int)checkdate.DayOfWeek;
            var checkTime = checkdate.TimeOfDay;
            var items = Items as List<LocationConfigurationItem>;
            return items.FindAll(i => i.DayOfWeek == checkDoW && i.StartTime <= checkTime && i.EndTime >= checkTime);
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string MinStartTimeString
        {
            get
            {
                var item = Items.OrderBy(i => i.StartTime).FirstOrDefault();
                if (item != null)
                    return item.StartTimeString;

                return "00:00";
            }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string MaxEndTimeString
        {
            get
            {
                var item = Items.OrderByDescending(i => i.EndTime).FirstOrDefault();
                if (item != null)
                    return item.EndTimeAdjusted;
                return "23:00";
            }
        }
    }

    public class LocationConfigurationItem
    {
        public static string[] DayOfWeekList = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

        public int ID { get; set; }
        public int LocationConfigurationID { get; set; }
        public int DayOfWeek { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string DayOfWeekString
        {
            get { return DayOfWeekList[this.DayOfWeek]; }
        }
        [Display(Name = "Start")]
        public TimeSpan StartTime { get; set; }
        [Display(Name = "End")]
        public TimeSpan EndTime { get; set; }
        public int Slots { get; set; }
        [Display(Name = "Duration")]
        public int SlotDuration { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string StartTimeString { get { return String.Format("{0}:{1:00}", StartTime.Hours, StartTime.Minutes); } }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string EndTimeString { get { return String.Format("{0}:{1:00}", EndTime.Hours, EndTime.Minutes); } }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string EndTimeAdjusted { get { return String.Format("{0}:{1:00}", EndTime.Hours + 1, EndTime.Minutes); } }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string TimeRangeView { get { return String.Format("{0} - {1}", this.StartTimeString, this.EndTimeString); } }
    }

    public class LocationBlackoutSlots
    {
        public int ID { get; set; }
        public int LocationID { get; set; }
        public virtual Location Location { get; set; }
        public int Slots { get; set; }
        [Display(Name = "Start")]
        public DateTime StartTime { get; set; }
        [Display(Name = "End")]
        public DateTime EndTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string StartTimeString { get { return StartTime.ToString("MM/dd/yyyy HH:mm"); } }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string EndTimeString { get { return EndTime.ToString("MM/dd/yyyy HH:mm"); } }
    }

    public class LocationWhitelistSlots
    {
        public int ID { get; set; }
        public int LocationID { get; set; }
        public virtual Location Location { get; set; }
        public int Slots { get; set; }
        [Display(Name = "Start")]
        public DateTime StartTime { get; set; }
        [Display(Name = "End")]
        public DateTime EndTime { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string StartTimeString { get { return StartTime.ToString("MM/dd/yyyy HH:mm"); } }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string EndTimeString { get { return EndTime.ToString("MM/dd/yyyy HH:mm"); } }
    }
}