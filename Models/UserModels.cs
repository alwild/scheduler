﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Scheduler.Models
{
    public class UserProfile
    {
        [Key]
        public string UserId { get; set; }
        public int? LocationID { get; set; }
        public virtual Location Location { get; set; }
        public string Name { get; set; }
        public string BillTo { get; set; }
        public string TMWDriverID { get; set; }
        public string RevType1 { get; set; }
    }

    public class UserProfileViewModel
    {
        public UserProfileViewModel() { }

        public UserProfileViewModel(ApplicationUser user, UserProfile profile, IEnumerable<IdentityRole> roles)
        {
            Email = user.Email;
            UserId = user.Id;
            if (profile != null)
            {
                Name = profile.Name;
                if (profile.Location != null)
                {
                    LocationID = profile.LocationID;
                    LocationName = profile.Location.Name;
                }
                BillTo = profile.BillTo;
                TMWDriverID = profile.TMWDriverID;
                RevType1 = profile.RevType1;
            }
            var role = user.Roles.FirstOrDefault();
            if (role != null)
            {
                RoleName = roles.First(r => r.Id == role.RoleId).Name;
                RoleID = role.RoleId;
            }
        }

        public string UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string RoleID { get; set; }
        public string RoleName { get; set; }
        public int? LocationID { get; set; }
        public string LocationName { get; set; }
        public string BillTo { get; set; }
        public string TMWDriverID { get; set; }
        public string RevType1 { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$", 
            ErrorMessage = "The {0} must be at least 6 characters long and contain a combination of uppercase, lowercase and numbers and one special character")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}