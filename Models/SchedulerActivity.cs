﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scheduler.Models
{
    public class SchedulerActivity
    {
        public int ID { get; set; }

        public string ActivityType { get; set; }

        public DateTime ActivityDate { get; set; }

        public string ActivityUserID { get; set; }
        public int EntityID { get; set; }
        public string PreviousValue { get; set; }
        public string NewValue { get; set; }

        public static void AppointmentRescheduled(DateTime old_date, DateTime new_date, string userid, int appointmentid)
        {
            using (var db = new DAL())
            {
                var activity = new SchedulerActivity()
                {
                    ActivityType = "Appointment Rescheduled",
                    ActivityDate = DateTime.Now,
                    ActivityUserID = userid,
                    EntityID = appointmentid,
                    PreviousValue = old_date.ToString(),
                    NewValue = new_date.ToString()
                };

                db.SchedulerActivities.Add(activity);
                db.SaveChanges();
            }
        }
    }
}