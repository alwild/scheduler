﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Scheduler.Models
{
    public class Location
    {
        public int ID { get; set; }
        public string Name { get; set; }
        [Index(IsUnique = true)]
        [StringLength(255)]
        public string Slug { get; set; }
        [DataType(DataType.MultilineText)]
        public string TMWCode { get; set; }
        
        [StringLength(255)]
        public string TMWCityName { get; set; }

        [NotMapped]
        public List<string> TMWCodes
        {
            get
            {
                if (!String.IsNullOrEmpty(TMWCode))
                {
                    return new List<string>(TMWCode.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries));
                }
                else
                {
                    return new List<string>();
                }
            }

            set
            {
                TMWCode = String.Join("\r\n", value);
            }
        }
    }

    public class LocationDetailsViewModel
    {
        public Location Location { get; set; }
        public LocationConfiguration CurrentConfig { get; set; }
        public List<LocationConfiguration> Configs { get; set; }
        public List<LocationBlackoutSlots> Blackout { get; set; }
        public List<LocationWhitelistSlots> Whitelist { get; set; }
    }
}