namespace Scheduler.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public class DAL : DbContext
    {
        // Your context has been configured to use a 'DAL' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Scheduler.Models.DAL' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'DAL' 
        // connection string in the application configuration file.
        public DAL()
            : base("name=DAL")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Carrier> Carriers { get; set; }
        public virtual DbSet<Status> Statuses { get; set; }
        public virtual DbSet<Appointment> Appointments { get; set; }
        public virtual DbSet<AppointmentRecurringSettings> AppointmentRecurringSettings { get; set; }
        public virtual DbSet<LocationConfiguration> LocationConfiguration { get; set; }
        public virtual DbSet<LocationConfigurationItem> LocationConfigurationItem { get; set; }
        public virtual DbSet<LocationBlackoutSlots> LocationBlackoutSlots { get; set; }
        public virtual DbSet<LocationWhitelistSlots> LocationWhitelistSlots { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<SchedulerActivity> SchedulerActivities { get; set; }

        public LocationConfiguration GetCurrentConfiguration(int location_id, LocationConfigurationType config_type=0)
        {
            var item = this.LocationConfiguration.Where(l => l.LocationID == location_id    
                && l.PublishDate <= DateTime.Now && l.ConfigurationType == config_type)
                .OrderByDescending(l => l.PublishDate).FirstOrDefault();
            return item;
        }
    }

    public class DALInitializer : DropCreateDatabaseIfModelChanges<DAL>
    {
        protected override void Seed(DAL context)
        {
            base.Seed(context);
        }
    }
}