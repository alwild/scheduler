﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Scheduler.App_Code
{
    public static class MvcHtmlHelpers
    {
        public static MvcHtmlString ShortNameFor<TModel, TValue>(this HtmlHelper<TModel> self,
                Expression<Func<TModel, TValue>> expression)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, self.ViewData);
            var name = metadata.ShortDisplayName ?? metadata.DisplayName ?? metadata.PropertyName;

            return MvcHtmlString.Create(string.Format(@"<span>{0}</span>", name));
        }
    }
}