﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Scheduler.Models;


namespace Scheduler.Controllers
{
    public class BaseController : Controller
    {
        private Scheduler.Models.DAL db = new Scheduler.Models.DAL();
        private ApplicationUserManager _userManager;

        public BaseController() { }

        public BaseController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            var inIframe = false;
            if (Request.QueryString["iframe"] == "1")
            {
                inIframe = true;
            }
            if (!String.IsNullOrEmpty(Request.QueryString["ReturnUrl"]) &&
                Request.QueryString["ReturnUrl"].Contains("iframe=1"))
            {
                inIframe = true;
            }

            if(Session["InIframe"]!= null && Convert.ToBoolean(Session["InIframe"]) == true)
            {
                inIframe = true;
            }

            this.ViewBag.InIframe = inIframe;
            if (inIframe)
            {
                Session["InIframe"] = inIframe;
            }
            if (Request.IsAuthenticated)
            {
                this.ViewBag.NavItems = Scheduler.Models.ViewModels.NavItem.GetNavItemsForRole(UserManager.GetRoles(User.Identity.GetUserId()));
            }
            
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        protected UserProfile GetCurrentUserProfile()
        {
            var userid = User.Identity.GetUserId();
            var profile = db.UserProfiles.FirstOrDefault(p => p.UserId == userid);
            return profile;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }
            }

            base.Dispose(disposing);
        }
    }
}