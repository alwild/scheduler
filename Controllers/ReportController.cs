﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Scheduler.Models;
using Scheduler.Helpers;
using System.Web.Script.Serialization;


namespace Scheduler.Controllers
{
    [Authorize]
    public class ReportController : BaseController
    {
        Scheduler.Models.DAL db = new Scheduler.Models.DAL();
        ApplicationDbContext app_db = new ApplicationDbContext();
        // GET: Report
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult LocationSchedule(Models.ViewModels.LocationScheduleForm model, string export)
        {
            var locations = db.Locations;
            ViewBag.Status = new SelectList(Models.TMW.TMWOrderStatus.Get(), "ord_status", "ord_status", model.Status);
            ViewBag.AppointmentType = new SelectList(Models.TMW.TMWAppointmentType.Get(), "app_type", "app_type", model.AppointmentType);
            ViewBag.City = new SelectList(locations, "TMWCityName", "Name", model.City);
            if (model.Start.HasValue && model.End.HasValue)
            {
                var report = Models.TMW.DockReportModel.GetReport(model.Start.Value, model.End.Value, model.City, model.Status, model.AppointmentType);
                if (!String.IsNullOrEmpty(export))
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=schedule.xlsx");
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                    var wb = new ClosedXML.Excel.XLWorkbook();
                    var sheet = wb.AddWorksheet("report");
                    ReportHelper.SetRowValues(sheet, 1, new string[] { "Date", "Order Number", "STO Number", "Carrier", "Status", "Appointment Type" }, bold:true);
                    var row_idx = 2;
                    foreach (var item in report)
                    {
                        var report_data = new string[] { "'" + item.Date.ToString("MM/dd/yyyy HH:mm"), item.OrderNumber, item.STONumber, item.Carrier, item.Status, item.AppointmentType };
                        if (item.ConsolidatedSubOrder)
                        {
                            report_data = new string[] { "", item.OrderNumber, item.STONumber, "", "", "" };
                        }
                        ReportHelper.SetRowValues(sheet, row_idx, report_data, bold:item.ConsolidatedFirstOrder);
                        row_idx++;
                        if (item.HasChildOrders)
                        {
                            ReportHelper.SetRowValues(sheet, row_idx, new string[] { "", "Order Number", "STO Number", "Carrier", "Status", "Appointment Type" }, bold:true);
                            row_idx++;
                            foreach(var sub_item in item.ChildOrders)
                            {
                                report_data = new string[] { "", sub_item.SubOrderNumber, sub_item.SubSTONumber, sub_item.SubCarrier, sub_item.SubStatus, sub_item.SubAppointmentType };
                                ReportHelper.SetRowValues(sheet, row_idx, report_data);
                                row_idx++;
                            }
                        }
                    }

                    wb.SaveAs(Response.OutputStream);

                    Response.Flush();
                    Response.End();
                    return null;
                }

                ViewBag.ReportData = report;
                var json = new JavaScriptSerializer().Serialize(report);
                ViewBag.JsonData = json;
                return View(model);
            }


            return View(model);
        }

        public ActionResult CarrierPaySchedule(int order_number = 0)
        {
            var items = Models.TMW.CarrierPayScheduleReport.GetReport(order_number);
            return View(items);
        }

        public ActionResult LoadStatus(string search)
        {
            if (!String.IsNullOrEmpty(search))
            {
                var po_numbers = search.Split(new char[] { '\n' }).Select(a => a.Trim()).ToArray();
                var profile = GetCurrentUserProfile();
                string billto = null;
                if (profile != null)
                {
                    billto = profile.BillTo;
                }
                var items = Models.TMW.LoadStatusReport.GetReport(po_numbers, billto);
                return View(items);
            }
            return View(new List<Models.TMW.LoadStatusReport>());
        }

        public ActionResult DriverMileage(DateTime? start, DateTime? end)
        {
            var profile = GetCurrentUserProfile();
            string driverid = null;
            if (profile != null)
            {
                driverid = profile.TMWDriverID;
            }
            if (start.HasValue && end.HasValue)
            {
                var reportItems = Models.TMW.MileageReport.GetReport(start.Value, end.Value, driverid);
                return View(reportItems);
            }
            return View();
        }
    }
}