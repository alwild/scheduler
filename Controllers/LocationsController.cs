﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scheduler.Models;
using Scheduler.App_Code;

namespace Scheduler.Controllers
{
    [Authorize(Roles = "Admin,Super Admin")]
    public class LocationsController : BaseController
    {
        private Scheduler.Models.DAL db = new Scheduler.Models.DAL();

        // GET: Locations
        public ActionResult Index()
        {
            return View(db.Locations.ToList());
        }

        // GET: Locations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            LocationConfiguration config = db.LocationConfiguration.Where(l => l.LocationID == id && l.PublishDate <= DateTime.Now)
                .OrderByDescending(l => l.PublishDate).FirstOrDefault();

            List<LocationBlackoutSlots> blackoutslots = db.LocationBlackoutSlots.Where(l => l.LocationID == id)
                .OrderByDescending(l => l.StartTime).ToList();

            List<LocationWhitelistSlots> whitelist = db.LocationWhitelistSlots.Where(l => l.LocationID == id)
                .OrderByDescending(l => l.StartTime).ToList();

            List<LocationConfiguration> configs = db.LocationConfiguration.Where(l => l.LocationID == id)
                .OrderByDescending(l => l.PublishDate).ToList();

            LocationDetailsViewModel model = new LocationDetailsViewModel()
            {
                Location = location,
                CurrentConfig = config,
                Configs = configs,
                Blackout = blackoutslots,
                Whitelist = whitelist
            };

            if (location == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // GET: Locations/Create
        [Authorize(Roles = "Super Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Locations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Super Admin")]
        public ActionResult Create([Bind(Include = "ID,Name,TMWCode,TMWCityName")] Location location)
        {
            if (ModelState.IsValid)
            {
                location.Slug = location.Name.GenerateSlug();
                db.Locations.Add(location);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(location);
        }

        // GET: Locations/Edit/5
        [Authorize(Roles = "Super Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // POST: Locations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Super Admin")]
        public ActionResult Edit([Bind(Include = "ID,Name,TMWCode,TMWCityName")] Location location)
        {
            if (ModelState.IsValid)
            {
                location.Slug = location.Name.GenerateSlug();
                db.Entry(location).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(location);
        }

        // GET: Locations/Delete/5
        [Authorize(Roles = "Super Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // POST: Locations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Super Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Location location = db.Locations.Find(id);
            db.Locations.Remove(location);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBlackoutSlot(LocationBlackoutSlots blackout)
        {
            if (ModelState.IsValid)
            {
                db.LocationBlackoutSlots.Add(blackout);
                db.SaveChanges();
                return Json(new { result = "success" });
            }
            return Json(new { result = "failure" });
        }

        [HttpGet]
        public ActionResult EditBlackoutSlot(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LocationBlackoutSlots item = db.LocationBlackoutSlots.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditBlackoutSlot(LocationBlackoutSlots blackout)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blackout).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { result = "success" });
            }

            return Json(new { result = "failure" });
        }

        [HttpGet]
        public ActionResult RemoveBlackoutSlot(int? id)
        {
            var slot = db.LocationBlackoutSlots.First(b => b.ID == id);
            db.LocationBlackoutSlots.Remove(slot);
            db.SaveChanges();
            return Json(new { result = "success" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddWhitelistSlot(LocationWhitelistSlots item)
        {
            if (ModelState.IsValid)
            {
                db.LocationWhitelistSlots.Add(item);
                db.SaveChanges();
                return Json(new { result = "success" });
            }
            return Json(new { result = "failure" });
        }

        [HttpGet]
        public ActionResult EditWhitelistSlot(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LocationWhitelistSlots item = db.LocationWhitelistSlots.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return Json(item, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditWhitelistSlot(LocationWhitelistSlots item)
        {
            if (ModelState.IsValid)
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { result = "success" });
            }

            return Json(new { result = "failure" });
        }

        [HttpGet]
        public ActionResult RemoveWhitelistSlot(int? id)
        {
            var slot = db.LocationWhitelistSlots.First(b => b.ID == id);
            db.LocationWhitelistSlots.Remove(slot);
            db.SaveChanges();
            return Json(new { result = "success" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddConfiguration(LocationConfiguration config)
        {
            if (ModelState.IsValid)
            {
                db.LocationConfiguration.Add(config);
                db.SaveChanges();
                return Json(new { result = "success" });
            }

            return Json(new { result = "failure", error = "Invalid values" });
        }

        [HttpGet]
        public ActionResult EditConfiguration(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LocationConfiguration config = db.LocationConfiguration.Find(id);
            if (config == null)
            {
                return HttpNotFound();
            }
            return Json(config, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditConfiguration(LocationConfiguration config)
        {
            if (ModelState.IsValid)
            {
                foreach(var item in config.Items)
                {
                    if (item.ID == 0)
                    {
                        db.LocationConfigurationItem.Add(item);
                    }
                    else
                    {
                        db.Entry(item).State = EntityState.Modified;
                    }
                }
                db.SaveChanges();
                db.Entry(config).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { result = "success" });
            }

            return Json(new { result = "failure" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddConfigurationItem(LocationConfigurationItem item)
        {
            if (ModelState.IsValid)
            {
                db.LocationConfigurationItem.Add(item);
                db.SaveChanges();
                return Json(new { result = "success" });
            }

            return Json(new { result = "failure", error = "Invalid values" });
        }

        [HttpGet]
        public ActionResult EditConfigurationItem(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LocationConfigurationItem config = db.LocationConfigurationItem.Find(id);
            if (config == null)
            {
                return HttpNotFound();
            }
            return Json(config, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditConfigurationItem(LocationConfigurationItem config)
        {
            if (ModelState.IsValid)
            {
                db.Entry(config).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { result = "success" });
            }

            return Json(new { result = "failure" });
        }

        [HttpGet]
        public ActionResult DeleteConfiguration(int? id)
        {
            var config = db.LocationConfiguration.Find(id);
            db.LocationConfiguration.Remove(config);
            db.SaveChanges();
            return Json(new { result = "success" }, JsonRequestBehavior.AllowGet);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
