﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scheduler.Models;

namespace Scheduler.Controllers
{
    [Authorize]
    public class CalendarController : BaseController
    {
        Scheduler.Models.DAL db = new Scheduler.Models.DAL();
        ApplicationDbContext app_db = new ApplicationDbContext();

        string[] SetAppointmentTimeRoles = new string[] { "Admin", "Planner", "Super Admin" };

        // GET: Calendar
        public ActionResult Index(string loc, int configtype=0)
        {
            return View("Obsolete");
            ViewBag.StatusID = new SelectList(db.Statuses, "ID", "Name");
            ViewBag.LocationID = new SelectList(db.Locations, "Slug", "Name", loc);
            ViewBag.CarrierID = new SelectList(db.Carriers, "ID", "Name");
            ViewBag.ConfigurationType = configtype;

            var location = db.Locations.FirstOrDefault(l => l.Slug == loc);
            if (location == null)
            {
                location = db.Locations.First();
            }

            var user = app_db.Users.First(u => u.UserName == User.Identity.Name);
            var profile = db.UserProfiles.FirstOrDefault(p => p.UserId == user.Id);
            if (profile != null && profile.Location != null)
            {
                location = profile.Location;
            }

            ViewBag.Location = location;
            ViewBag.MinStartTimeString = "00:00";
            ViewBag.MaxEndTimeString = "23:00";
            var configuration = db.GetCurrentConfiguration(location.ID, (LocationConfigurationType)configtype);
            if (configuration != null)
            {
                ViewBag.MinStartTimeString = configuration.MinStartTimeString;
                ViewBag.MaxEndTimeString = configuration.MaxEndTimeString;
            }
            return View();
        }

        public IEnumerable<Status> GetStatuses()
        {
            var statuses = db.Statuses.ToList();
            if (User.IsInRole("DockWorker"))
            {
                var doc_worker_statues = new string[] { "Late", "Complete" };
                statuses = statuses.Where(s => doc_worker_statues.Contains(s.Name)).ToList();
            }
            return statuses;
        }

        public ActionResult Order(string order, string t="ord", string stop_number = "0", string configtype="LUL")
        {
            var locations = db.Locations.ToList();
            ViewBag.StatusID = new SelectList(GetStatuses(), "ID", "Name");
            ViewBag.LocationID = new SelectList(locations, "Slug", "Name");
            ViewBag.CarrierID = new SelectList(db.Carriers, "ID", "Name");
            ViewBag.Order = new Appointment();
            ViewBag.MinStartTimeString = "00:00";
            ViewBag.MaxEndTimeString = "23:00";
            ViewBag.CanUpdateTime = "true";

            if (User.IsInRole("DC Employee") || User.IsInRole("ViewOnly"))
            {
                ViewBag.CanUpdateTime = "false";
            }

            try
            {
                var profile = this.GetCurrentUserProfile();
                var tmw_events = Scheduler.Models.TMW.TMWOrderEvent.GetStops(order, t, revtype1: profile.RevType1);

                //Consolidated orders will have several order numbers in the "batch" of stops.  Only schedule the first order
                order = Convert.ToString(tmw_events.Min(a => a.ord_hdrnumber));
                foreach (var evt in tmw_events)
                {
                    evt.ScheduleLocation = locations.FirstOrDefault(l => l.TMWCodes.Contains(Convert.ToString(evt.stp_city))
                                                                    || l.TMWCodes.Contains(evt.stp_companyid));
                }

                var cfgtype_index = Array.IndexOf(Appointment.TMWAppointmentType, configtype);
                //gotta find the location id for the order
                var current_event = tmw_events.FirstOrDefault(e => e.stp_number == Convert.ToInt32(stop_number));
                if (current_event == null)
                {
                    current_event = tmw_events.FirstOrDefault(e => e.ScheduleLocation != null);
                }
                if (current_event == null)
                {
                    current_event = tmw_events.FirstOrDefault();
                }

                if (current_event == null)
                {
                    throw new Exception("Could not find order in TMW");
                }

                var location = current_event.ScheduleLocation;
                var tmw_orders = Models.TMW.TMWOrder.OrdersInMove(current_event.move_number);
                ViewBag.LocationID = new SelectList(locations, "Slug", "Name", location);

                String stp_number = Convert.ToString(current_event.stp_number);
                var current_appointment = db.Appointments.FirstOrDefault(a => a.OrderNumber == order && a.StopNumber == stp_number);
                if (current_appointment == null)
                {
                    current_appointment = new Appointment()
                    {
                        StartTime = current_event.evt_earlydate,
                        EndTime = current_event.evt_latedate,
                        LocationID = (location != null)?location.ID:0,
                        Location = location,
                        OrderNumber = order,
                        StopNumber = Convert.ToString(current_event.stp_number),
                        AppointmentType = (AppointmentType)cfgtype_index,
                        StatusID = db.Statuses.FirstOrDefault(s => s.Name == "Scheduled").ID
                    };
                }

                ViewBag.CurrentEvent = current_event;
                ViewBag.Order = current_appointment;
                ViewBag.STONumber = current_event.sto_number;
                ViewBag.ConfigurationType = 0;
                ViewBag.OrderNumber = order;
                ViewBag.StopNumber = current_appointment.StopNumber;
                ViewBag.TMWEvents = tmw_events;
                ViewBag.LocationSlug = (location != null) ? location.Slug : "";
                ViewBag.StartDate = current_appointment.StartTime.Date;
                ViewBag.OrderNumberDisplay = String.Join(", ", tmw_orders.Select(to => to.ord_hdrnumber));
                ViewBag.STONumberDisplay = String.Join(", ", tmw_orders.Select(to => to.ord_refnum));
                ViewBag.CarrierDisplay = String.Join(", ", tmw_orders.Select(to => to.ord_carrier));

                LocationConfiguration configuration = null;
                if (location != null)
                {
                    configuration = db.GetCurrentConfiguration(location.ID, (LocationConfigurationType)cfgtype_index);
                }
                
                if (configuration != null)
                {
                    ViewBag.MinStartTimeString = configuration.MinStartTimeString;
                    ViewBag.MaxEndTimeString = configuration.MaxEndTimeString;
                }
                return View(current_appointment);
            }
            catch(Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
        }

        public ActionResult ExternalOrder(string LocationID, string configtype = "LUL", 
            string criteria = null, string search_type="ord")
        {
            ViewBag.SearchCriteria = criteria;
            bool order_found = true;
            Appointment order = null;
            if (search_type == "ord") {
                order = db.Appointments.FirstOrDefault(a => a.IsExternal == true && a.OrderNumber == criteria);
            }
            if (search_type == "ref")
            {
                order = db.Appointments.FirstOrDefault(a => a.IsExternal == true && a.STONumber == criteria);
            }
            string location_slug = LocationID;
            var locations = db.Locations.ToList();
            if (order == null)
            {
                order = new Appointment();
                order_found = false;  //suck a dumb hack, but i'm in a hurry now
            }
            else
            {
                location_slug = order.Location.Slug;
                configtype = Appointment.TMWAppointmentType[(int)order.AppointmentType];
            }
            ViewBag.StatusID = new SelectList(GetStatuses(), "ID", "Name");
            ViewBag.LocationID = new SelectList(locations, "Slug", "Name");
            ViewBag.CarrierID = new SelectList(db.Carriers, "ID", "Name");
            ViewBag.Order = order;
            ViewBag.MinStartTimeString = "00:00";
            ViewBag.MaxEndTimeString = "23:00";
            ViewBag.CanUpdateTime = "true";
            ViewBag.StartDate = DateTime.Today;
            if (order_found)
            {
                ViewBag.StartDate = order.StartTime.Date;
            }

            if (User.IsInRole("DockWorker"))
            {
                ViewBag.CanUpdateTime = "false";
            }

            try
            {
                var cfgtype_index = Array.IndexOf(Appointment.TMWAppointmentType, configtype);
                var location = locations.FirstOrDefault(l => l.Slug == location_slug);
                if (location == null)
                {
                    location = locations.First();
                }

                ViewBag.LocationID = new SelectList(locations, "Slug", "Name", location_slug);

                order.LocationID = location.ID;
                ViewBag.ConfigurationTypeString = configtype;
                ViewBag.ConfigurationType = cfgtype_index;
                ViewBag.LocationSlug = location.Slug;
                ViewBag.Location = location;

                var configuration = db.GetCurrentConfiguration(location.ID, (LocationConfigurationType)cfgtype_index);
                if (configuration != null)
                {
                    ViewBag.MinStartTimeString = configuration.MinStartTimeString;
                    ViewBag.MaxEndTimeString = configuration.MaxEndTimeString;
                }
                return View(order);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
        }


        [JsonException]
        public JsonResult GetAppointments(string location, int config_type, DateTime start, DateTime end)
        {
            if (String.IsNullOrEmpty(location))
            {
                return Json(new List<Appointment>());
            }
            
            var appointmentLocation = db.Locations.Where(l => l.Slug == location).First();
            var locationId = appointmentLocation.ID;
            var starttime = adjustStartTime(appointmentLocation, start);
            var endtime = end.AddMinutes(30);
            var appointments = db.Appointments.Where(a => a.LocationID == locationId
                && a.AppointmentType == (AppointmentType)config_type
                && a.StartTime >= starttime
                && a.EndTime <= endtime).ToList();
            //get the TMW info for TMW orders.  STO and Carrier are stored in the DB for External Order, but TWM manages the STO and carrier for NON-External Orders
            var tmw_appointments = appointments.Where(a => a.IsExternal.GetValueOrDefault() == false);
            if (tmw_appointments.Count() > 0)
            {
                Models.TMW.TMWOrder.FillInTMWInfo(tmw_appointments);
            }

            var schedule = fillInAvailableSlots(appointmentLocation, (LocationConfigurationType)config_type, starttime, endtime, appointments);

            return Json(schedule);
        }

        private DateTime adjustStartTime(Location loc, DateTime start)
        {
            //in order to get the calendar to get the start time right on the hour, it is necessary to move it
            //half an hour.  unless it is midnight
            if (start.Minute == 0 && start.Hour > 0)
            {
                return start.AddMinutes(30);
            }

            return start;
        }

        private IList<Appointment> fillInAvailableSlots(Location loc, LocationConfigurationType config_type, DateTime start, DateTime end, IList<Appointment> appointments)
        {
            var appointmentMap = getAppointmentMap(loc, config_type, start, end, appointments);
            var schedule = getAppointmentSchedule(appointmentMap, appointments);

            return schedule;
        }

        private MappedSchedule getAppointmentMap(Location loc, LocationConfigurationType config_type, DateTime start, DateTime end, IList<Appointment> appointments)
        {
            var config = db.GetCurrentConfiguration(loc.ID, config_type);
            if (config == null)
            {
                throw new Exception(String.Format("No schedule found for {0} : {1}", loc.Name, config_type));
            }
            var whiteout = db.LocationWhitelistSlots.Where(w => w.LocationID == loc.ID &&  
                    ( (w.StartTime >= start && w.StartTime <= end) || (w.EndTime >= start && w.EndTime <= end) ) ).ToList();
            var blackout = db.LocationBlackoutSlots.Where(w => w.LocationID == loc.ID &&
                    ( (w.StartTime >= start && w.StartTime <= end) || (w.EndTime >= start && w.EndTime <= end) ) ).ToList();
            var appointmentMap = getAppointmentSlots(config, whiteout, blackout, loc.ID, start, end);

            return appointmentMap;
        }

        private MappedSchedule getAppointmentSlots(LocationConfiguration config, 
            List<LocationWhitelistSlots> whiteout, List<LocationBlackoutSlots> blackout, 
            int locationId, DateTime start, DateTime end)
        {
            int i = -1;
            var appointmentMap = new MappedSchedule();
            DateTime current = start;
            while (current < end)
            {
                if (current >= DateTime.Today)
                {
                    var items = config.GetSlots(current);
                    if (items.Count > 0)
                    {
                        foreach (var item in items)
                        {
                            for (int slot = 0; slot < item.Slots; slot++)
                            {
                                var available = new Appointment()
                                {
                                    ID = i,
                                    StartTime = current,
                                    EndTime = current.AddMinutes(item.SlotDuration),
                                    OrderNumber = "Available",
                                    LocationID = locationId,
                                    AppointmentType = (AppointmentType)config.ConfigurationType,
                                    Status = new Status()
                                    {
                                        Color = "#0000FF"
                                    }
                                };
                                i = i - 1;

                                addAppointmentSlot(appointmentMap, available);
                            }
                            current = current.AddMinutes(item.SlotDuration);
                        }
                    }
                    else
                    {
                        current = current.AddMinutes(15);
                    }
                }
                else
                {
                    current = current.AddMinutes(15);
                }
            }

            //add in whitelist
            foreach (var item in whiteout)
            {
                for (DateTime white_current = item.StartTime; white_current <= item.EndTime; white_current = white_current.AddHours(1))
                {
                    for (int slot = 0; slot < item.Slots; slot++)
                    {
                        var available = new Appointment()
                        {
                            ID = i,
                            StartTime = white_current,
                            EndTime = white_current.AddHours(1),
                            OrderNumber = "Available",
                            LocationID = locationId,
                            AppointmentType = (AppointmentType)config.ConfigurationType,
                            Status = new Status()
                            {
                                Color = "#0000FF"
                            }
                        };
                        i = i - 1;

                        addAppointmentSlot(appointmentMap, available);
                    }
                }
            } 
            
            //remove blacklist
            foreach (var item in blackout)
            {
                for (DateTime black_current = item.StartTime; black_current <= item.EndTime; black_current = black_current.AddMinutes(15))
                {
                    if (item.Slots == 0)
                    {
                        //just remove ALL the items
                        removeAppointmentSlot(appointmentMap, black_current, true);
                    }
                    else
                    {
                        for (int j=0; j < item.Slots; j++)
                        {
                            removeAppointmentSlot(appointmentMap, black_current, false);
                        }
                    }
                }
            }

            return appointmentMap;
        }

        private IList<Appointment> getAppointmentSchedule(MappedSchedule map, IList<Appointment> appointments)
        {
            var schedule = new List<Appointment>();

            foreach(var appointment in appointments)
            {
                schedule.Add(appointment);
                var slots = map.GetAppointments(appointment.StartTime);
                if(slots != null && slots.Count > 0 && !appointment.IsCanceled)
                {
                    //remove an available slot
                    slots.RemoveAt(0);
                }
            }

            foreach(var slots in map.Values)
            {
                foreach(var available in slots)
                {
                    schedule.Add(available);
                }
            }

            return schedule;
        }

        private void removeAppointmentSlot(MappedSchedule map, DateTime start, bool all)
        {
            var appointments = map.GetAppointments(start);
            if (appointments != null && appointments.Count > 0)
            {
                if (all)
                {
                    appointments.Clear();
                }
                else
                {
                    appointments.RemoveAt(0);
                }
            }
        }

        private void addAppointmentSlot(MappedSchedule map, Appointment appointment)
        {
            var appointments = map.GetAppointments(appointment.StartTime);
            if (appointments == null)
            {
                appointments = new List<Appointment>();
                map.Add(appointment.StartTime, appointments);
            }

            appointments.Add(appointment);
        }

        private bool ValidateAppointment(Appointment newAppointment, Appointment oldAppointment)
        {
            if (User.IsInRole("Admin") || User.IsInRole("Planner") || User.IsInRole("Super Admin"))
            {
                return true;
            }

            if (User.IsInRole("DC Employee"))
            {
                //dock workers can only update the status and comments.  not the appointment time
                if (oldAppointment != null && newAppointment.StartTime != oldAppointment.StartTime && newAppointment.IsExternal != true)
                {
                    throw new Exception("You do not have permissions to update the appointment time");
                }
            }

            if (User.IsInRole("View"))
            {
                throw new Exception("You do not have permissions to update appointments");
            }

            if (newAppointment.IsRecurring == true && !newAppointment.RecurringSettings.FinalDate.HasValue)
            {
                throw new Exception("You must specify the final date of the recurring appointment");
            }

            return true;
        }

        [JsonException]
        public JsonResult SetAppointment(Appointment appointment)
        {
            var existing_appointment = db.Appointments.FirstOrDefault(a => a.OrderNumber == appointment.OrderNumber && a.StopNumber == appointment.StopNumber);
            if (ModelState.IsValid && isAvailable(appointment, existing_appointment))
            {
                if (appointment.IsRecurring == false)
                {
                    appointment.RecurringSettings = null;
                    appointment.RecurringSettingsID = null;
                }
                //let's make sure the user has the role that can edit the info
                ValidateAppointment(appointment, existing_appointment);

                if (appointment.ID < 0 && existing_appointment == null)
                {
                    appointment.ID = 0;
                    appointment.CreatedDate = DateTime.Now;
                    appointment.CreatedBy = User.Identity.Name;
                    db.Entry(appointment).State = System.Data.Entity.EntityState.Added;
                }
                else
                {
                    if (existing_appointment != null)
                    {
                        //this preserves some fields that are not edited in the order form.
                        var old_date = existing_appointment.StartTime;
                        var new_date = appointment.StartTime;
                        existing_appointment.CopyFrom(appointment);
                        existing_appointment.ModifiedDate = DateTime.Now;
                        existing_appointment.ModifiedBy = User.Identity.Name;

                        if (old_date != new_date)
                        {
                            SchedulerActivity.AppointmentRescheduled(old_date, new_date, User.Identity.Name, existing_appointment.ID);
                        }
                    }
                    else
                    {
                        appointment.ModifiedDate = DateTime.Now;
                        appointment.ModifiedBy = User.Identity.Name;
                        db.Entry(appointment).State = System.Data.Entity.EntityState.Modified;
                    }
                }
                if (!appointment.IsExternal.HasValue || appointment.IsExternal == false)
                {
                    var stop = Models.TMW.TMWOrderEvent.GetStop(Convert.ToInt32(appointment.StopNumber));

                    Models.TMW.TMWOrderEvent.UpdateOrderStops(Convert.ToInt32(appointment.OrderNumber),
                            stop.evt_eventcode, stop.cty_nmstct, appointment.StartTime, appointment.ArrivalDateTime, appointment.DepartureDateTime,
                            appointment.Comments);
                }
                var recurring_settings = appointment.RecurringSettings;

                if (appointment.IsRecurring == true)
                {
                    var recurring_appointment_id = appointment.RecurringSettingsID;
                    if (existing_appointment != null && existing_appointment.RecurringSettingsID != null)
                    {
                        recurring_appointment_id = existing_appointment.RecurringSettingsID;
                        var existing_recurring_settings = existing_appointment.RecurringSettings;
                        existing_recurring_settings.UpdateFrom(recurring_settings);
                        recurring_settings = existing_recurring_settings;
                    }

                    recurring_settings.StartTime = appointment.StartTime;
                    recurring_settings.EndTime = appointment.EndTime;
                    appointment.RecurringSettings = recurring_settings;
                    appointment.RecurringSettingsID = UpdateRecurring(appointment, recurring_settings, recurring_appointment_id);
                }

                db.SaveChanges();
                return Json(new { result = "success" });
            }

            throw new Exception("Could not set appointment.  Please check that you are setting it to an available and future time slot");
        }

        private int UpdateRecurring(Appointment appointment, AppointmentRecurringSettings recurring_settings, int? recurring_settings_id)
        {
            var end_date = appointment.StartTime.AddYears(1);
            if (recurring_settings_id == null || recurring_settings_id == 0)
            {
                var recurring_schedule = recurring_settings;
                db.AppointmentRecurringSettings.Add(recurring_schedule);
                db.SaveChanges();
                recurring_settings_id = recurring_schedule.ID;
            }
            else
            {
                //remove all the future recurring appointments associated with this schedule
                db.Appointments.RemoveRange(db.Appointments.Where(a => a.RecurringSettingsID == recurring_settings_id && a.StartTime > appointment.StartTime));
                var recurring_schedule = recurring_settings;
                db.Entry(recurring_schedule).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            //setup a new recurring schedule based on this appointment
            foreach(var next_apt_dates in recurring_settings.GetDates())
            {
                var next_start = next_apt_dates[0];
                var next_end = next_apt_dates[1];
                var next_appointment = new Appointment();
                next_appointment.CopyFrom(appointment);
                next_appointment.CopyDates(appointment);
                var order_number = appointment.OrderNumber.Split(new char[] { ':' })[0];
                next_appointment.OrderNumber = String.Format("{0}:{1:yyyyMMdd}", order_number, next_start);
                next_appointment.StartTime = next_start;
                next_appointment.EndTime = next_end;
                next_appointment.RecurringSettingsID = recurring_settings_id;
                db.Appointments.Add(next_appointment);
            }
            db.SaveChanges();
            return recurring_settings_id.Value;
        }

        private bool isAvailable(Appointment appointment, Appointment existing_appointment)
        {
            if (existing_appointment != null && appointment.StartTime == existing_appointment.StartTime)
            {
                //something other than the time has changed...this is fine
                return true;
            }

            if (appointment.StartTime <= DateTime.Now)
            {
                return false;
            }

            var locationId = appointment.LocationID;
            var starttime = appointment.StartTime;
            var endtime = appointment.EndTime;
            var appointmentLocation = db.Locations.First(l => l.ID == appointment.LocationID);

            var appointments = db.Appointments.AsNoTracking().Where(a => a.LocationID == locationId
                && a.AppointmentType == appointment.AppointmentType
                && a.StartTime >= starttime 
                && a.EndTime <= endtime).ToList();

            var appointmentMap = getAppointmentMap(appointmentLocation, (LocationConfigurationType)appointment.AppointmentType, starttime, endtime, appointments);
            var schedule = getAppointmentSchedule(appointmentMap, appointments);

            //i believe if there is still an available slot left in the map then we are good to set the appointment
            if (appointmentMap.Keys.Contains(starttime))
            {
                var availableSlots = appointmentMap[starttime];
                return (availableSlots.Count() > 0);
            }

            return false;
        }
    }

    public class JsonExceptionAttribute : FilterAttribute, IExceptionFilter
    {
        public virtual void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            if (filterContext.Exception != null)
            {
                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.Clear();
                filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
                filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                filterContext.Result = new JsonResult() { Data = filterContext.Exception.Message };
            }
        }
    }
}