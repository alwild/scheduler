﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Scheduler.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Scheduler.Controllers
{
    [Authorize(Roles = "Admin,Super Admin")]
    public class UsersController : BaseController
    {
        ApplicationDbContext db = new ApplicationDbContext();
        Scheduler.Models.DAL dal = new Scheduler.Models.DAL();

        // GET: Users
        public ActionResult Index()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var roleList = roleManager.Roles.ToList();
            var userList = new List<UserProfileViewModel>();
            var userProfiles = dal.UserProfiles.ToList();
            foreach(var user in db.Users.ToList())
            {
                var profile = userProfiles.FirstOrDefault(p => p.UserId == user.Id);
                var userModel = new UserProfileViewModel(user, profile, roleList);
                userList.Add(userModel);
            }
            return View(userList);
        }

        public ActionResult Details(string id)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var roleList = roleManager.Roles.ToList();
            var user = db.Users.FirstOrDefault(f => f.Id == id);
            var profile = dal.UserProfiles.FirstOrDefault(p => p.UserId == id);
            var userProfile = new UserProfileViewModel(user, profile, roleList);

            return View(userProfile);
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var roleList = roleManager.Roles.ToList();
            var user = db.Users.FirstOrDefault(f => f.Id == id);
            var profile = dal.UserProfiles.FirstOrDefault(p => p.UserId == id);
            var userProfile = new UserProfileViewModel(user, profile, roleList);
            ViewBag.LocationID = new SelectList(dal.Locations, "ID", "Name", userProfile.LocationID);
            ViewBag.RoleID = new SelectList(db.Roles, "ID", "Name", userProfile.RoleID);

            return View(userProfile);
        }

        [HttpPost]
        public ActionResult Edit(string id, UserProfileViewModel profileView)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var user = db.Users.FirstOrDefault(f => f.Id == profileView.UserId);
            if (ModelState.IsValid)
            {
                var roles = user.Roles.Select(r => r.RoleId).ToArray();
                var role = roleManager.FindById(profileView.RoleID);
                UserManager.RemoveFromRoles(profileView.UserId, roles);
                UserManager.AddToRole(profileView.UserId, role.Name);
                
                var profile = dal.UserProfiles.FirstOrDefault(p => p.UserId == profileView.UserId);
                if (profile == null)
                {
                    profile = new UserProfile();
                    profile.UserId = profileView.UserId;
                    profile.LocationID = profileView.LocationID;
                    profile.Name = profileView.Name;
                    profile.BillTo = profileView.BillTo;
                    profile.TMWDriverID = profileView.TMWDriverID;
                    profile.RevType1 = profileView.RevType1;
                    dal.UserProfiles.Add(profile);
                }
                else
                {
                    profile.LocationID = profileView.LocationID;
                    profile.Name = profileView.Name;
                    profile.BillTo = profileView.BillTo;
                    profile.TMWDriverID = profileView.TMWDriverID;
                    profile.RevType1 = profileView.RevType1;
                }
                dal.SaveChanges();

                if (!String.IsNullOrEmpty(profileView.Password))
                {
                    var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    string token = userManager.GeneratePasswordResetToken(user.Id);
                    userManager.ResetPassword(user.Id, token, profileView.Password);
                }

                return RedirectToAction("Details", new { id = profileView.UserId });
            }
            ViewBag.LocationID = new SelectList(dal.Locations, "ID", "Name", profileView.LocationID);
            ViewBag.RoleID = new SelectList(db.Roles, "ID", "Name", profileView.RoleID);
            return View(profileView);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.LocationID = new SelectList(dal.Locations, "ID", "Name");
            ViewBag.RoleID = new SelectList(db.Roles, "ID", "Name");
            return View();
        }

        [HttpPost]
        public ActionResult Create(UserProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var result = userManager.Create(user, model.Password);
                if (result.Succeeded)
                {
                    var profile = new UserProfile();
                    profile.UserId = user.Id;
                    profile.LocationID = model.LocationID;
                    profile.Name = model.Name;
                    profile.BillTo = model.BillTo;
                    profile.TMWDriverID = model.TMWDriverID;
                    profile.RevType1 = model.RevType1;
                    dal.UserProfiles.Add(profile);
                    dal.SaveChanges();
                    if (!String.IsNullOrEmpty(model.RoleID))
                    {
                        var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                        var role = roleManager.FindById(model.RoleID);
                        UserManager.AddToRole(user.Id, role.Name);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ErrorMessage = String.Join("<br/>", result.Errors);
                }
            }
            ViewBag.LocationID = new SelectList(dal.Locations, "ID", "Name", model.LocationID);
            ViewBag.RoleID = new SelectList(db.Roles, "ID", "Name", model.RoleID);
            return View(model);
        }

        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            var user = UserManager.FindById(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ApplicationUser user = UserManager.FindById(id);
            UserManager.Delete(user);
            return RedirectToAction("Index");
        }
    }
}