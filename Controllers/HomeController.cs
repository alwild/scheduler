﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Scheduler.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            var location = LoginLocation();
            if (location != null)
            {
                return location;
            }
            var model = new Scheduler.Models.LoginViewModel();
            return View(model);
        }

        private ActionResult LoginLocation()
        {
            if (User.Identity.IsAuthenticated && true.Equals(Session["InIframe"]))
            {
                return RedirectToAction("LoadStatus", "Report");
            }
            else if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("LocationSchedule", "Report");
            }
            return null;
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Terms()
        {
            return View();
        }
    }
}