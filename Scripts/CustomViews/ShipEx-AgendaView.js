﻿(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'moment'], factory);
    }
    else if (typeof exports === 'object') { // Node/CommonJS
        module.exports = factory(require('jquery'), require('moment'));
    }
    else {
        factory(jQuery, moment);
    }
})(function ($, moment) {
    ;;
    var SE = SE | {};
    var FC = $.fullCalendar;

    var fcViews = FC.views;
    fcViews.shipex = {};

    var ShipEXDayTableMixin = SE.DayTableMixin = FC.DayTableMixin;

    var dayIDs = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
    var intervalUnits = ['year', 'month', 'week', 'day', 'hour', 'minute', 'second', 'millisecond'];

    function htmlEscape(s) {
        return (s + '').replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/'/g, '&#039;')
            .replace(/"/g, '&quot;')
            .replace(/\n/g, '<br />');
    }

    ShipEXDayTableMixin.renderHeadDateCellHtml = function (date, colspan, otherAttrs) {
        var view = this.view;

        return '' +
            '<th class="fc-day-header ' + view.widgetHeaderClass + ' fc-' + dayIDs[date.day()] + '"' +
                (this.rowCnt == 1 ?
                    ' data-date="' + date.format('YYYY-MM-DD') + '"' :
                    '') +
                (colspan > 1 ?
                    ' colspan="' + colspan + '"' :
                    '') +
                (otherAttrs ?
                    ' ' + otherAttrs :
                    '') +
            '>' +
                htmlEscape(date.format(this.colHeadFormat)) +
            '</th>';
    } 

    var ShipEXDayGrid = SE.DayGrid = FC.DayGrid.extend(ShipEXDayTableMixin, {

    });

    var ShipEXTimeGrid = SE.TimeGrid = FC.TimeGrid.extend(ShipEXDayTableMixin, {

    });

    var ShipEXAgendaView = SE.AgendaView = FC.AgendaView.extend({

        dayGridClass: ShipEXDayGrid,
        timeGridClass: ShipEXTimeGrid,
        renderHead: function () {
            FC.AgendaView.prototype.renderHead.apply(this, arguments);
        }
    });

    fcViews.shipex_agenda = {
        'class': ShipEXAgendaView,
        defaults: {
            allDaySlot: true,
            allDayText: 'all-day',
            slotDuration: '00:30:00',
            minTime: '00:00:00',
            maxTime: '24:00:00',
            slotEventOverlap: true // a bad name. confused with overlap/constraint system
        }
    };

    fcViews.shipex_agendaDay = {
        type: 'shipex_agenda',
        duration: { days: 1 }
    };

    fcViews.shipex_agendaWeek = {
        type: 'shipex_agenda',
        duration: { weeks: 1 }
    };

    ;;
});