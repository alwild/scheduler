﻿$(document).ready(function () {

    //setup the creation dialog
    $('#create-blackoutslot-dialog').dialog({
        autoOpen: false,
        width: "310px",
        title: "New Blackout",
        buttons: [
            {
                text: 'Cancel',
                click: function () {
                    $(this).dialog('close');
                }
            },
            {
                text: 'Save',
                click: function () {
                    var id = $('#location-id').val();
                    var starttime = $('#create-blackoutslot-start-time').val();
                    var endtime = $('#create-blackoutslot-end-time').val();
                    var slots = $('#create-blackoutslot-slots').val();
                    var token = $('#create-blackoutslot-dialog input[name="__RequestVerificationToken"]').val();
                    var data = {
                        LocationID: id,
                        StartTime: starttime,
                        EndTime: endtime,
                        Slots: slots,
                        __RequestVerificationToken: token
                    };

                    $.post(BASE_URL + "Locations/AddBlackoutSlot", data, function (result) {
                        alert(result.result);
                        if (result.result == "success") {
                            $('#create-blackoutslot-dialog').dialog('close');
                            window.location.reload();
                        }
                    });
                }
            }
        ]
    });

    $('#add-blackoutslot').on('click', function () {
        $('#create-blackoutslot-dialog').dialog('open');
        return false;
    })


    //setup the edit dialog
    $('#edit-blackoutslot-dialog').dialog({
        autoOpen: false,
        width: "310px",
        title: "Edit Blackout",
        buttons: [
            {
                text: 'Cancel',
                click: function () {
                    $(this).dialog('close');
                }
            },
            {
                text: 'Save',
                click: function () {
                    var id = $("#edit-blackoutslot-id").val();
                    var locationid = $('#location-id').val();
                    var starttime = $('#edit-blackoutslot-start-time').val();
                    var endtime = $('#edit-blackoutslot-end-time').val();
                    var slots = $('#edit-blackoutslot-slots').val();
                    var token = $('#edit-blackoutslot-dialog input[name="__RequestVerificationToken"]').val();
                    var data = {
                        ID: id,
                        LocationID: locationid,
                        StartTime: starttime,
                        EndTime: endtime,
                        Slots: slots,
                        __RequestVerificationToken: token
                    };

                    $.post("/Locations/EditBlackoutSlot", data, function (result) {
                        alert(result.result);
                        if (result.result == "success") {
                            $('#edit-blackoutslot-dialog').dialog('close');
                            window.location.reload();
                        }
                    });
                }
            }
        ]
    });

    $('.edit-blackoutslot').on('click', function () {
        var id = $(this).attr("data-id");
        $.get(BASE_URL + "Locations/EditBlackoutSlot/" + id, function (result) {
            $("#edit-blackoutslot-id").val(id);
            $('#edit-blackoutslot-start-time').val(result.StartTimeString);
            $('#edit-blackoutslot-end-time').val(result.EndTimeString);
            $('#edit-blackoutslot-slots').val(result.Slots);
            $('#edit-blackoutslot-dialog').dialog('open');
        });
        return false;
    });

    $('.remove-blackoutslot').on('click', function () {
        var id = $(this).attr("data-id");
        $.get(BASE_URL + "Locations/RemoveBlackoutSlot", { "id": id }, function (result) {
            alert(result.result);
            if (result.result == 'success') {
                window.location.reload();
            }
        });

        return false;
    });
});