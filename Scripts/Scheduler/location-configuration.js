﻿$(document).ready(function () {

    var days_of_week = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

    //setup tabindexes
    $.each(days_of_week, function (i, o) {
        var day = o;
        var day_idx = i;

        var rows = 3;
        var items = 12;
        for(var i = 1;i <= rows; i++)
        {
            $("#" + day + "-start-" + i, "#create-locationconfiguration-dialog").attr("tabindex", items * day_idx + i);
            $("#" + day + "-end-" + i, "#create-locationconfiguration-dialog").attr("tabindex", items * day_idx + i);
            $("#" + day + "-slots-" + i, "#create-locationconfiguration-dialog").attr("tabindex", items * day_idx + i);
            $("#" + day + "-duration-" + i, "#create-locationconfiguration-dialog").attr("tabindex", items * day_idx + i);

            $("#" + day + "-start-" + i, "#edit-locationconfiguration-dialog").attr("tabindex", items * day_idx + i);
            $("#" + day + "-end-" + i, "#edit-locationconfiguration-dialog").attr("tabindex", items * day_idx + i);
            $("#" + day + "-slots-" + i, "#edit-locationconfiguration-dialog").attr("tabindex", items * day_idx + i);
            $("#" + day + "-duration-" + i, "#edit-locationconfiguration-dialog").attr("tabindex", items * day_idx + i);
        }
    });

    //setup the creation dialog
    $('#create-locationconfiguration-dialog').dialog({
        autoOpen: false,
        width: "365px",
        title: "New Configuration",
        buttons: [
            {
                text: 'Cancel',
                click: function () {
                    $(this).dialog('close');
                }
            },
            {
                text: 'Save',
                click: function () {
                    var id = $('#location-id').val();
                    var rows = 3;
                    var item_data = [];
                    for (var day_idx in days_of_week) {
                        var day = days_of_week[day_idx];
                        for (var idx = 1; idx <= rows; idx++) {
                            var starttime = $('#' + day + '-start-' + idx, '#create-locationconfiguration-items').val();
                            var endtime = $('#' + day + '-end-' + idx, '#create-locationconfiguration-items').val();
                            var slots = $('#' + day + '-slots-' + idx, '#create-locationconfiguration-items').val();
                            var duration = $('#' + day + '-duration-' + idx, '#create-locationconfiguration-items').val();
                            if (starttime != '') {
                                item_data.push({
                                    DayOfWeek: day_idx,
                                    StartTime: starttime,
                                    EndTime: endtime,
                                    Slots: slots,
                                    SlotDuration: duration
                                });
                            }
                        }
                    }

                    var publish_date = $('#create-locationconfiguration-publish-date').val();
                    var config_type = $('#create-locationconfiguration-type').val();
                    var token = $('#create-locationconfiguration-dialog input[name="__RequestVerificationToken"]').val();
                    var data = {
                        LocationID: id,
                        Items: item_data,
                        PublishDate: publish_date,
                        ConfigurationType: config_type,
                        __RequestVerificationToken: token
                    };

                    $.post(BASE_URL + "/Locations/AddConfiguration", data, function (result) {
                        alert(result.result);
                        if (result.result == "success") {
                            $('#create-locationconfiguration-dialog').dialog('close');
                            window.location.reload();
                        }
                    });
                }
            }
        ]
    });

    $('#add-configuration').on('click', function () {
        $('#create-locationconfiguration-dialog').dialog('open');
        return false;
    })


    //setup the edit dialog
    $('#edit-locationconfiguration-dialog').dialog({
        autoOpen: false,
        width: "365px",
        title: "Edit Configuration",
        buttons: [
            {
                text: 'Cancel',
                click: function () {
                    $(this).dialog('close');
                }
            },
            {
                text: 'Save',
                click: function () {
                    var id = $("#edit-configuration-id").val();
                    var locationid = $('#location-id').val();

                    var rows = 3;
                    var item_data = [];
                    for (var day_idx in days_of_week) {
                        var day = days_of_week[day_idx];
                        for (var idx = 1; idx <= rows; idx++) {
                            var starttime = $('#' + day + '-start-' + idx, '#edit-locationconfiguration-items').val();
                            var endtime = $('#' + day + '-end-' + idx, '#edit-locationconfiguration-items').val();
                            var slots = $('#' + day + '-slots-' + idx, '#edit-locationconfiguration-items').val();
                            var duration = $('#' + day + '-duration-' + idx, '#edit-locationconfiguration-items').val();
                            var item_id =$('#' + day + '-item-id-' + idx, '#edit-locationconfiguration-items').val();
                            if (item_id == 'undefined' || item_id == '') {
                                item_id = 0;
                            }
                            if (starttime != '') {
                                item_data.push({
                                    DayOfWeek: day_idx,
                                    StartTime: starttime,
                                    EndTime: endtime,
                                    Slots: slots,
                                    SlotDuration: duration,
                                    ID: item_id,
                                    LocationConfigurationID: id
                                });
                            }
                        }
                    } 

                    var publish_date = $('#edit-locationconfiguration-publish-date').val();
                    var config_type = $('#edit-locationconfiguration-type').val();
                    var token = $('#edit-locationconfiguration-dialog input[name="__RequestVerificationToken"]').val();
                    var data = {
                        ID: id,
                        LocationID: locationid,
                        PublishDate: publish_date,
                        Items: item_data,
                        ConfigurationType: config_type,
                        __RequestVerificationToken: token
                    };

                    $.post(BASE_URL + "Locations/EditConfiguration", data, function (result) {
                        alert(result.result);
                        if (result.result == "success") {
                            $('#edit-locationconfiguration-dialog').dialog('close');
                            window.location.reload();
                        }
                    });
                }
            }
        ]
    });

    $('.edit-configuration').on('click', function () {
        var id = $(this).attr("data-id");
        $.get(BASE_URL + "Locations/EditConfiguration/" + id, function (result) {
            $("#edit-configuration-id").val(id);
            $('#edit-locationconfiguration-publish-date').val(result.PublishDateString);
            $('#edit-locationconfiguration-type').val(result.ConfigurationType);

            var days_counters = {};
            for (var day_idx in days_of_week) {
                var day = days_of_week[day_idx];
                days_counters[day] = 1;
            }

            for (var idx in result.Items) {
                var item = result.Items[idx];
                var day = days_of_week[parseInt(item.DayOfWeek)];
                var day_counter = days_counters[day]++;

                $('#' + day + '-start-' + day_counter, '#edit-locationconfiguration-items').val(item.StartTimeString);
                $('#' + day + '-end-' + day_counter, '#edit-locationconfiguration-items').val(item.EndTimeString);
                $('#' + day + '-slots-' + day_counter, '#edit-locationconfiguration-items').val(item.Slots);
                $('#' + day + '-duration-' + day_counter, '#edit-locationconfiguration-items').val(item.SlotDuration);
                $('#' + day + '-item-id-' + day_counter, '#edit-locationconfiguration-items').val(item.ID)
            }

            $('#edit-locationconfiguration-dialog').dialog('open');
        });
        return false;
    });

    $('.delete-configuration').on('click', function () {
        var id = $(this).attr("data-id");
        $.get(BASE_URL + "Locations/DeleteConfiguration/" + id, function (result) {
            alert(result.result);
            window.location.reload();
        });
        return false;
    });
});