﻿$(document).ready(function () {

    //setup the creation dialog
    $('#create-whitelistslot-dialog').dialog({
        autoOpen: false,
        width: "310px",
        title: "New Whitelist",
        buttons: [
            {
                text: 'Cancel',
                click: function () {
                    $(this).dialog('close');
                }
            },
            {
                text: 'Save',
                click: function () {
                    var id = $('#location-id').val();
                    var starttime = $('#create-whitelistslot-start-time').val();
                    var endtime = $('#create-whitelistslot-end-time').val();
                    var slots = $('#create-whitelistslot-slots').val();
                    var token = $('#create-whitelistslot-dialog input[name="__RequestVerificationToken"]').val();
                    var data = {
                        LocationID: id,
                        StartTime: starttime,
                        EndTime: endtime,
                        Slots: slots,
                        __RequestVerificationToken: token
                    };

                    $.post(BASE_URL + "Locations/AddWhitelistSlot", data, function (result) {
                        alert(result.result);
                        if (result.result == "success") {
                            $('#create-whitelistslot-dialog').dialog('close');
                            window.location.reload();
                        }
                    });
                }
            }
        ]
    });

    $('#add-whitelistslot').on('click', function () {
        $('#create-whitelistslot-dialog').dialog('open');
        return false;
    })


    //setup the edit dialog
    $('#edit-whitelistslot-dialog').dialog({
        autoOpen: false,
        width: "310px",
        title: "Edit Whitelist",
        buttons: [
            {
                text: 'Cancel',
                click: function () {
                    $(this).dialog('close');
                }
            },
            {
                text: 'Save',
                click: function () {
                    var id = $("#edit-whitelistslot-id").val();
                    var locationid = $('#location-id').val();
                    var starttime = $('#edit-whitelistslot-start-time').val();
                    var endtime = $('#edit-whitelistslot-end-time').val();
                    var slots = $('#edit-whitelistslot-slots').val();
                    var token = $('#edit-whitelistslot-dialog input[name="__RequestVerificationToken"]').val();
                    var data = {
                        ID: id,
                        LocationID: locationid,
                        StartTime: starttime,
                        EndTime: endtime,
                        Slots: slots,
                        __RequestVerificationToken: token
                    };

                    $.post(BASE_URL + "Locations/EditWhitelistSlot", data, function (result) {
                        alert(result.result);
                        if (result.result == "success") {
                            $('#edit-whitelistslot-dialog').dialog('close');
                            window.location.reload();
                        }
                    });
                }
            }
        ]
    });

    $('.edit-whitelistslot').on('click', function () {
        var id = $(this).attr("data-id");
        $.get(BASE_URL + "Locations/EditWhitelistSlot/" + id, function (result) {
            $("#edit-whitelistslot-id").val(id);
            $('#edit-whitelistslot-start-time').val(result.StartTimeString);
            $('#edit-whitelistslot-end-time').val(result.EndTimeString);
            $('#edit-whitelistslot-slots').val(result.Slots);
            $('#edit-whitelistslot-dialog').dialog('open');
        });
        return false;
    });

    $('.remove-whitelistslot').on('click', function () {
        var id = $(this).attr("data-id");
        $.get(BASE_URL + "Locations/RemoveWhitelistSlot", { "id": id }, function (result) {
            alert(result.result);
            if (result.result == 'success') {
                window.location.reload();
            }
        });
    });
});